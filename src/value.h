#pragma once

#include <variant>
#include <string>
#include <iomanip>
#include <iostream>
#include <type_traits>
#include <optional>
#include <exception>


// helper constant for the visitor #3
template<class> inline constexpr bool always_false_v = false;

// helper type for the visitor #4
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

// disambiguation from long...
enum class Bool : bool
{
    False = false,
    True = true
};

struct DebugInfo
{
    DebugInfo();

    DebugInfo(size_t row, size_t col);

    size_t row;
    size_t col;
};

class Value
{
    using ValueType = std::variant<std::monostate, Bool, long, double, std::string>;

public:
    static Value null;

    enum class Type
    {
        Null, Bool, Int, Float, String
    };
    static constexpr std::array<Type, 5> listTypes { Type::Null, Type::Bool, Type::Int, Type::Float, Type::String };

    Value() = default;

    template<typename T>
    Value(const T &value)
        : m_value(value)
    {}

    static Value makeBool(bool b);

    Value(const Value &value) = default;
    Value(Value &value) = default;
    Value(Value &&value) = default;

    Value &operator=(const Value&) = default;
    friend std::ostream& operator<<(std::ostream& os, const Value &value);

    Type type() const;
    std::string typeStr() const;
    std::string toString() const;

    bool isBoolTrue() const;
    bool isBoolFalse() const;
    bool isNumber() const;

    const ValueType &value() const;

private:
    ValueType m_value;
};

std::ostream& operator<<(std::ostream& os, const Value &value);
