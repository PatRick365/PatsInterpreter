#pragma once

class Config
{
public:
    Config();

    Config &printStandAloneExpressions();
    Config &exitOnError();

    bool m_printStandAloneExpressions;
    bool m_exitOnError;
};
