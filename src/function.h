#pragma once

#include "value.h"
#include "error.h"
#include "result.h"

#include <string>
#include <vector>
#include <optional>
#include <functional>


class Function
{
public:
    using FuncType = std::function<Result<Value>(const std::vector<Value> &args, const DebugInfo &debugInfo)>;

    Function(const std::string &name, size_t argNum, bool returnVal, const FuncType &func);

    static bool exists(const std::string &name);
    static std::optional<Function> getFunction(const std::string &name);

    Result<Value> invoke(const std::vector<Value> &args, const DebugInfo &debugInfo)
    {
        if (args.size() != argNum)
            return Error(debugInfo, "function " + name + " expects " + std::to_string(argNum) + " argument" + ((argNum != 1) ? "s" : "") + ". recieved " + std::to_string(args.size()));

        return func(args, debugInfo);
    }

    std::string name;
    size_t argNum;
    bool returnVal;
    FuncType func;

    static std::vector<Function> list;
};


