#pragma once

#include "value.h"
#include "operators.h"

#include <memory>
#include <vector>


class Visitor;

class Expression
{
public:
    using Ptr = std::unique_ptr<Expression>;

    Expression(const DebugInfo &debugInfo);

    virtual ~Expression() = default;

    [[nodiscard]] virtual bool acceptExpression(Visitor *vistior) = 0;

    DebugInfo debugInfo;
};

class ExpOpBinary : public Expression
{
public:
    using Ptr = std::unique_ptr<ExpOpBinary>;

    static ExpOpBinary::Ptr make(Expression::Ptr lhv, OpBinary op, Expression::Ptr rhv, const DebugInfo &debugInfo);

    ExpOpBinary(Expression::Ptr lhv, OpBinary op, Expression::Ptr rhv, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptExpression(Visitor *vistior) override;

    OpBinary op;
    Expression::Ptr lhs;
    Expression::Ptr rhs;
};

class ExpValue : public Expression
{
public:
    using Ptr = std::unique_ptr<ExpValue>;

    static ExpValue::Ptr make(const Value &value, const DebugInfo &debugInfo);

    explicit ExpValue(const DebugInfo &debugInfo);
    ExpValue(const Value &value, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptExpression(Visitor *vistior) override;

    virtual ~ExpValue() = default;

    Value value;
};

class ExpLValue : public ExpValue
{
public:
    using Ptr = std::unique_ptr<ExpLValue>;

    static ExpLValue::Ptr make(const std::string &name, const DebugInfo &debugInfo);

    ExpLValue();
    ExpLValue(const std::string &name, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptExpression(Visitor *vistior) override;

    std::string name;
};

class ExpFunction : public Expression
{
public:
    using Ptr = std::unique_ptr<ExpFunction>;

    static ExpFunction::Ptr make(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo);

    ExpFunction(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptExpression(Visitor *vistior) override;

    std::string name;
    std::vector<Expression::Ptr> args;

};
