#include "ast.h"

#include "logging.h"
#include "value.h"
#include "operation.h"
#include "function.h"

#define IF_ERROR_SET_AND_RETURN(x) do { if (x.hasError()) { m_error = x.error(); return false; } } while(false)

#define forever for (;;)

Visitor::Visitor(const Config &config)
    : m_config(config)
{}


InterpreterVisitor::InterpreterVisitor(const Config &config, Stack &stack)
    : Visitor(config),
      m_stack(stack)
{}

bool InterpreterVisitor::visit(ExpOpBinary *bop)
{
    if (!bop->lhs->acceptExpression(this))
        return false;

    Value lhs = m_resultVal;

    if (!bop->rhs->acceptExpression(this))
        return false;

    Value rhs = m_resultVal;

    Result<Value> result;

    switch (bop->op)
    {
    case OpBinary::Minus:
        result = Evaluation::substract(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::Plus:
        result = Evaluation::add(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::Divide:
        result = Evaluation::divide(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::Multiplication:
        result = Evaluation::multiply(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::Equal:
        result = Evaluation::equal(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::NotEqual:
        result = Evaluation::notEqual(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::LessThan:
        result = Evaluation::lessThan(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::GreaterThan:
        result = Evaluation::greaterThan(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::LessThanOrEqual:
        result = Evaluation::lessOrEqual(lhs, rhs, bop->debugInfo);
        break;
    case OpBinary::GreaterThanOrEqual:
        result = Evaluation::greaterOrEqual(lhs, rhs, bop->debugInfo);
        break;
    }

    IF_ERROR_SET_AND_RETURN(result);

    m_resultVal = result.value();

    LOG_PRETTY(LogId::AstExecution, lhs.toString() + " " << toString(bop->op) + " " + rhs.toString() + " == " + m_resultVal.toString());

    return true;
}

bool InterpreterVisitor::visit(ExpValue *valueExp)
{
    LOG_PRETTY(LogId::AstExecution, "evaluated to: " << valueExp->value.toString());

    m_resultVal = valueExp->value;

    return true;
}

bool InterpreterVisitor::visit(ExpLValue *lVal)
{
    ExpLValue *lValFound = m_stack.findStackValue(lVal->name);

    if (lValFound != nullptr)
    {
        m_resultVal = lValFound->value;
    }
    else
    {
        m_error = Error(lVal->debugInfo, "unknown symbol");
        return false;
    }

    LOG_PRETTY(LogId::AstExecution, "evaluated to: " + lVal->name + " == " + m_resultVal.toString());

    return true;
}

bool InterpreterVisitor::visit(ExpFunction *function)
{
    std::vector<Value> args;

    for (Expression::Ptr &expr : function->args)
    {
        if (!expr->acceptExpression(this))
            return false;

        args.emplace_back(std::move(m_resultVal));
    }

    Result<Value> returnValue = Function::getFunction(function->name)->invoke(args, function->debugInfo);

    if (returnValue.hasError())
    {
        m_error = returnValue.error();
        return false;
    }

    m_resultVal = returnValue.value();

    LOG_PRETTY(LogId::AstExecution, "function " + function->name + " returned " + m_resultVal.toString());

    return true;
}

bool InterpreterVisitor::visit(StatNoOp *)
{
    return true;
}

bool InterpreterVisitor::visit(Statement *statement)
{
    return statement->acceptStatement(this);
}

bool InterpreterVisitor::visit(StatementBlock *statementBlock)
{
    AutoPushPopStack autoPushPopStack(m_stack); // pop stack on error or what?

    for (Statement::Ptr &statement : statementBlock->statements)
    {
        if (!statement->acceptStatement(this))
            return false;
    }

    return true;
}

bool InterpreterVisitor::visit(StatIfThenElse *ifThenElse)
{
    //AutoPopStack autoPopStack(m_stack);

    if (!ifThenElse->condition->acceptExpression(this))
        return false;

    LOG_PRETTY(LogId::AstExecution, "if " + m_resultVal.toString());

    Result<Value> result = Evaluation::equal(m_resultVal, Bool::True, ifThenElse->condition->debugInfo);

    IF_ERROR_SET_AND_RETURN(result);

    if (result.value().isBoolTrue())
    {
        if (!ifThenElse->ifBranch->acceptStatement(this))
            return false;
    }
    else if (ifThenElse->elseBranch != nullptr) // if has else branch
    {
        if (!ifThenElse->elseBranch->acceptStatement(this))
            return false;
    }

    return true;
}

bool InterpreterVisitor::visit(StatAssignment *assignment)
{
    if (!assignment->rValue->acceptExpression(this))
        return false;

    LOG_PRETTY(LogId::AstExecution, assignment->lValue->name << " " + toString(assignment->op) + " " + m_resultVal.toString());

    ExpLValue &lVal = m_stack.findOrCreateStackValue(assignment->lValue->name, assignment->lValue->debugInfo);

    Result<Value> result;

    switch (assignment->op)
    {
    case OpAssignment::Assign:
        lVal.value = m_resultVal;
        return true;
    case OpAssignment::MinusAssign:
        result = Evaluation::substract(lVal.value, m_resultVal, assignment->debugInfo);
        break;
    case OpAssignment::PlusAssign:
        result = Evaluation::add(lVal.value, m_resultVal, assignment->debugInfo);
        break;
    case OpAssignment::DivideAssign:
        result = Evaluation::divide(lVal.value, m_resultVal, assignment->debugInfo);
        break;
    case OpAssignment::MultiplyAssign:
        result = Evaluation::multiply(lVal.value, m_resultVal, assignment->debugInfo);
        break;
    }

    IF_ERROR_SET_AND_RETURN(result);

    lVal.value = result.value();

    return true;
}

bool InterpreterVisitor::visit(StatWhile *statWhile)
{
    AutoPopStack autoPopStack(m_stack);

    forever
    {
        if (!statWhile->condition->acceptExpression(this))
            return false;

        LOG_PRETTY(LogId::AstExecution, "while " << m_resultVal.toString());

        Result<Value> result = Evaluation::equal(m_resultVal, Bool::True, statWhile->debugInfo);

        IF_ERROR_SET_AND_RETURN(result);

        if (result.value().isBoolTrue())
        {            
            if (!statWhile->body->acceptStatement(this))
                return false;
        }
        else
        {
            return true;
        }
    }
    return true;
}

bool InterpreterVisitor::visit(StatFor *statFor)
{
    if (!statFor->initStatement->acceptStatement(this))
        return false;

    forever
    {
        if (!statFor->condition->acceptExpression(this))
            return false;

        Result<Value> result = Evaluation::equal(m_resultVal, Bool::True, statFor->condition->debugInfo);

        IF_ERROR_SET_AND_RETURN(result);

        if (result.value().isBoolTrue())
        {
            if (!statFor->body->acceptStatement(this))
                return false;
        }
        else
        {
            break;
        }

        if (!statFor->iterationExpression->acceptStatement(this))
            return false;
    }
    return true;
}

bool InterpreterVisitor::visit(StatPrint *statPrint)
{
    if (!statPrint->expression->acceptExpression(this))
        return false;

    std::cout << m_resultVal << "\n";

    return true;
}

bool InterpreterVisitor::visit(StatFunction *statFunction)
{
    std::vector<Value> args;

    for (Expression::Ptr &expr : statFunction->args)
    {
        if (!expr->acceptExpression(this))
            return false;

        args.emplace_back(std::move(m_resultVal));
    }

    // possibly returns null if void function
    Result<Value> returnValue = Function::getFunction(statFunction->name)->invoke(args, statFunction->debugInfo);

    if (returnValue.hasError())
    {
        m_error = returnValue.error();
        return false;
    }

    if (m_config.m_printStandAloneExpressions)
        std::cout << returnValue.value() << "\n";

    return true;
}

void InterpreterVisitor::reset()
{
    m_stack.m_scopes.clear();
}
