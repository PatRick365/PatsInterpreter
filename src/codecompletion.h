#pragma once

#include <vector>
#include <string>
#include <cstring>


extern char **completer(const char *text, int, int);

class CodeCompletion
{
private:
    CodeCompletion() = default;

public:
    CodeCompletion(const CodeCompletion &) = delete;
    CodeCompletion &operator=(const CodeCompletion &) = delete;
    CodeCompletion(CodeCompletion &&) = delete;
    CodeCompletion &operator=(CodeCompletion &&) = delete;

    static CodeCompletion &get()
    {
        static CodeCompletion instance;
        return instance;
    }

    void registerWord(const std::string &word);
    void unRegisterWord(const std::string &word);

    const std::vector<std::string> &vocabulary() const;

private:
    std::vector<std::string> m_vocabulary;

};



