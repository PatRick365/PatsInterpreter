#pragma once

//todo cleanup includes, do forward declarations

#include "config.h"
#include "stack.h"
#include "cmdparse.h"
#include "lexer.h"
#include "token.h"
#include "ast.h"
#include "sourcecodeinput.h"

#include <string>
#include <memory>
#include <queue>


class Interpreter
{
public:
    explicit Interpreter(const Config &config, ISourceCodeInput &sourceInput);

    int run();

    Result<std::vector<Token>> fetchTokenLine();

    Result<std::vector<Token>::const_iterator> findStatement(TokenIterator itBegin, TokenIterator itEnd) const;

    Result<StatementBlock::Ptr> fetchStatementBlock();
    Result<Statement::Ptr> fetchStatement();

    std::optional<Error> errorCheckInput(const std::vector<Token> &tokens);
    Result<std::vector<Token>> expressionToPostFix(TokenIterator itBegin, TokenIterator itEnd);
    Result<Expression::Ptr> parseExpression(TokenIterator itBegin, TokenIterator itEnd, bool parenthesized);
    Result<std::vector<Expression::Ptr>> parseArgList(TokenIterator itBegin, TokenIterator itEnd);

    //std::optional<Error> lastError();

public:
    Config config;

    InterpreterVisitor interpreterVisitor;

    Stack m_stack;
    ISourceCodeInput &m_sourceInput;
    Lexer m_lexer;

    std::queue<std::vector<Token>> m_rejectedTokens;

    size_t m_indentCurrent = 0;
    bool m_atBlockBegin = true;

    std::optional<Error> lastError;

};

