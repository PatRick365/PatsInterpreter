#include "lexer.h"

#include "function.h"
#include "operators.h"
#include "sourcecodeinput.h"
#include "logging.h"

#include <algorithm>
#include <cstring>

#define forever for (;;)


static constexpr const char *indent = "    ";

Value tryParseValue(const std::string &str)
{
    char *p = nullptr;

    long i = std::strtol(str.data(), &p, 10);
    if (p == str.data() + str.size())
        return i;

    double d = std::strtod(str.data(), &p);
    if (p == str.data() + str.size())
        return d;

    return str;
}

std::optional<Error> errorCheckInput(size_t row, const std::string &line)
{
    {
        ssize_t openBracket = 0;

        for (size_t i = 0; i < line.size(); ++i)
        {
            if (line[i] == '(')
            {
                ++openBracket;
            }
            else if (line[i] == ')')
            {
                --openBracket;

                if (openBracket < 0)
                {
                    return Error(row, i + 1, "parentheses mismatch");
                }
            }
        }

        if (openBracket != 0)
            return Error(row, -1 + 1, "parentheses mismatch");
    }

    return std::nullopt;
}

Result<std::vector<Token>> Lexer::fetchLine(size_t row, const std::string &line)
{
    if (std::optional<Error> err = errorCheckInput(row, line))
    {
        return err.value();
    }

    Result<std::vector<TokenString>> result = split(row, line);
    if (result.hasError())
    {
        return result.error();
    }

    Result<std::vector<Token>> resultIndexed = indexLine(row, result.value());
    if (resultIndexed.hasError())
    {
        return resultIndexed.error();
    }

    return resultIndexed.value();

}

Result<std::vector<TokenString>> Lexer::split(size_t row, const std::string &line)
{
    LOG(LogId::Lexer, "lexer split input: '" << line << "'");

    std::vector<TokenString> words;

    auto addWord = [&words](const std::string &string, const DebugInfo &debugInfo)
    {
        LOG(LogId::Lexer, "addWord -> " << debugInfo.row << ":" << debugInfo.col << " '" << string);

        words.emplace_back(string, debugInfo);
    };

    std::size_t prev = 0;
    std::size_t pos = 0;

    static const std::vector<char> symbolsWidth1 { '(', ')', '<', '>', '-', '+', '/', '*', '=', ','};
    static const std::vector<std::string> symbolsWidth2 { "-=", "+=", "/=", "*=", "==", "!=", "<=", ">=" };

    // find indents
    forever
    {
        {
            size_t posIndent = line.find(indent, pos);
            if (posIndent == pos)
            {
                addWord(indent, DebugInfo(row, pos));
                pos += 4;
                prev = pos;
                continue;
            }
        }
        {
            size_t posIndent = line.find('\t', pos);
            if (posIndent == pos)
            {
//                return Error(row, pos + 1, "no tabs allowed");
                addWord(indent, DebugInfo(row, pos));
                pos += 1;
                prev = pos;
                continue;
            }
        }
        break;
    }

    while (pos <= line.size())
    {
        if (line[pos] == '\"')
        {
            if (pos > prev)
                addWord(line.substr(prev, pos - prev), DebugInfo(row, prev + 1));

            size_t posClose = line.find('\"', pos + 1);

            if (posClose == std::string::npos)
            {
                return Error(row, pos + 1, "no closing quotation mark to match this");
            }

            addWord(line.substr(pos, posClose - pos + 1), DebugInfo(row, pos + 1));
            pos = posClose + 1;
            prev = pos;
        }
        else if (line[pos] == ' ' || line[pos] == '\t'/* || pos == line.size() - 1*/)
        {
            if (pos > prev)
                addWord(line.substr(prev, pos - prev), DebugInfo(row, prev + 1));

            ++pos;
            prev = pos;
        }
        else if ((pos + 2 <= line.size()) && std::find(symbolsWidth2.begin(), symbolsWidth2.end(), line.substr(pos, 2)) != symbolsWidth2.end())
        {
            if (pos > prev)
                addWord(line.substr(prev, pos - prev), DebugInfo(row, prev + 1));

            addWord(line.substr(pos, 2), DebugInfo(row, pos + 1));
            pos += 2;
            prev = pos;
        }
        else if (std::find(symbolsWidth1.begin(), symbolsWidth1.end(), line[pos]) != symbolsWidth1.end())
        {
            if (pos > prev)
                addWord(line.substr(prev, pos - prev), DebugInfo(row, prev + 1));

            addWord(std::string(1, line[pos]), DebugInfo(row, pos + 1));
            pos += 1;
            prev = pos;
        }
        else
        {
            ++pos;
        }
    }

    std::string str = line.substr(prev, pos - prev);

    if (!str.empty())
        addWord(str, DebugInfo(row, prev + 1));

    return words;
}

Result<std::vector<Token> > Lexer::indexLine(size_t row, const std::vector<TokenString> &line)
{
    if (line.empty())
    {
        return std::vector<Token> { Token(TokenString("", DebugInfo(row, 1)), TokenType::Empty) };
    }

    std::vector<Token> tokens;

    for (const TokenString &tokenString : line)
    {
        if (tokenString.str == indent)
            tokens.push_back(Token(tokenString, TokenType::Indent));

        else if (tokenString.str == ",")
            tokens.push_back(Token(tokenString, TokenType::Comma));

//        else if (tokenString.str == "{")
//            tokens.push_back(Token(tokenString, TokenType::ScopeOpen));

//        else if (tokenString.str == "}")
//            tokens.push_back(Token(tokenString, TokenType::ScopeClose));

        else if (tokenString.str == "(")
            tokens.push_back(Token(tokenString, TokenType::ParenthesesOpen));

        else if (tokenString.str == ")")
            tokens.push_back(Token(tokenString, TokenType::ParenthesesClose));

        else if (tokenString.str == "if")
            tokens.push_back(Token(tokenString, TokenType::If));

        else if (tokenString.str == "else")
            tokens.push_back(Token(tokenString, TokenType::Else));

        else if (tokenString.str == "while")
            tokens.push_back(Token(tokenString, TokenType::While));

        else if (auto op = toOpBinary(tokenString.str))
            tokens.push_back(Token(tokenString, op.value()));

        else if (auto op = toOpAssignment(tokenString.str))
            tokens.push_back(Token(tokenString, op.value()));

        else
        {
            // find functions
            if (Function::exists(tokenString.str))
            {
                tokens.push_back(Token(tokenString, TokenType::Function));
            }
            else
            {
                // find 6 - 5.6 - "awd"
                if (tokenString.str.front() == '\"' && tokenString.str.back() == '\"')
                {
                    tokens.push_back(Token(tokenString, Value(tokenString.str.substr(1, tokenString.str.size() - 2))));
                }
                else
                {
                    Value value = tryParseValue(tokenString.str);

                    // is actually a name. strings are already parsed
                    if (value.type() == Value::Type::String)
                    {
                        if (tokenString.str == "true")
                        {
                            tokens.push_back(Token(tokenString, Value(Bool::True)));
                        }
                        else if (tokenString.str == "false")
                        {
                            tokens.push_back(Token(tokenString, Value(Bool::False)));
                        }
                        else
                        {
                            tokens.push_back(Token(tokenString, TokenType::Name));
                        }
                    }
                    else if (value.type() == Value::Type::Null)
                    {
                        return Error(tokenString.debugInfo, std::string("expression ") + tokenString.str + " evaluated to Null");
                    }
                    else
                    {
                        // is number
                        tokens.push_back(Token(tokenString, Value(value)));
                    }
                }
            }
        }
    }

    if (Logging::get().isOn(LogId::Lexer))
    {
        LOG(LogId::Lexer, "indexed tokens:");
        for (const Token &token: tokens)
        {
            LOG_PURE(LogId::Lexer, std::string("  ") + token.toString() + "\n");
        }
    }

    return tokens;
}
