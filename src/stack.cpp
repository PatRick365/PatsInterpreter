#include "stack.h"
#include "logging.h"
#include "codecompletion.h"


//Stack::Stack()
//{
//    push();
//}

AutoPopStack::AutoPopStack(Stack &stack)
    : m_stack(stack)
{}

AutoPopStack::~AutoPopStack()
{
    m_stack.pop();
}

AutoPushPopStack::AutoPushPopStack(Stack &stack)
    : m_stack(stack)
{}

AutoPushPopStack::~AutoPushPopStack()
{
    m_stack.pop();
}

Stack::Stack()
    : m_scopes(1)
{}

Scope::~Scope()
{
    for (const auto &namedValue : namedValues)
    {
        CodeCompletion::get().unRegisterWord(namedValue.first);
        //std::cout << "deleting " << namedValue.first << "\n";
    }
}

Scope::Scope(const Scope &other)
{
    for (const auto &namedValue : other.namedValues)
    {
        CodeCompletion::get().registerWord(namedValue.first);
    }
    namedValues = other.namedValues;
}

void Stack::push()
{
    m_scopes.push_back(Scope());

    if (Logging::get().isOn(LogId::Stack))
        std::cout << __PRETTY_FUNCTION__ << " size: " << m_scopes.size() << "\n";
}

void Stack::pop()
{
    // global stack stays
    if (m_scopes.size() > 1)
    {
        if (Logging::get().isOn(LogId::Stack))
        {
            std::string names;
            for (auto vals : m_scopes.back().namedValues)
            {
                names += vals.first + " ";
            }

            LOG(LogId::Stack, "popped values " + names);
        }

        // not eneugh because of scope copy
//        for (const auto &scope : m_scopes)
//        {
//            for (const auto &namedValue : scope.namedValues)
//            {
//                CodeCompletion::get().unRegisterWord(namedValue.first);
//            }
//        }

        m_scopes.pop_back();

        if (Logging::get().isOn(LogId::Stack))
            std::cout << __PRETTY_FUNCTION__ << " size: " << m_scopes.size() << "\n";
    }
}


void Stack::dump() const
{
    std::cout << "\n\n=====>STACK-DUMP<======================\n\n";

    for (size_t i = 0; i < m_scopes.size(); ++i)
    {
        for (const auto &namedValue : m_scopes[i].namedValues)
        {
            std::cout << std::string(' ', i * 4) << namedValue.second.value.typeStr() << " " << namedValue.second.name << ": " << namedValue.second.value << "\n";
            //std::cout << std::string(' ', i * 4) << namedValue.second.value.typeStr() << " " << namedValue.first << ": " << namedValue.second.value << "\n";
        }
    }

    std::cout << "\n=======================================\n\n";

}

ExpLValue &Stack::findOrCreateStackValue(const std::string &name, const DebugInfo &debugInfo)
{
    ExpLValue *found = findStackValue(name);

    if (found != nullptr)
        return *found;

    return createStackValue(name, debugInfo);
}

ExpLValue *Stack::findStackValue(const std::string &name)
{
    // start search from top stack
    for (auto itStack = m_scopes.rbegin(); itStack != m_scopes.rend(); ++itStack)
    {
        auto &namedValues = itStack->namedValues;

        auto it = namedValues.find(name);

        if (it != namedValues.end())
        {
            return &namedValues[name];
        }
    }
    return nullptr;
}

bool Stack::hasStackValue(const std::string &name)
{
    return (findStackValue(name) != nullptr);
}

ExpLValue &Stack::createStackValue(const std::string &name, const DebugInfo &debugInfo)
{
    if (m_scopes.empty())
    {
        m_scopes.resize(1);
        throw "awdd2";
    }

    LOG(LogId::Stack, "created value " + name);

    CodeCompletion::get().registerWord(name);

    return m_scopes.back().namedValues[name] = ExpLValue(name, debugInfo);
}

size_t Stack::valueCount() const
{
    size_t count = 0;

    for (const Scope &scope : m_scopes)
        count += scope.namedValues.size();

    return count;
}

//Stack2::Stack2()
//{}

//ExpLValue &Stack2::findOrCreateStackValue(const std::string &name)
//{
//    if (auto value = findStackValue(name))
//    {
//        return *value;
//    }
//    return createStackValue(name);
//}

//ExpLValue *Stack2::findStackValue(const std::string &name)
//{
//    for (auto &value : m_values)
//    {
//        if (value.name == name)
//            return &value;
//    }
//    return nullptr;
//}

//ExpLValue &Stack2::createStackValue(const std::string &name)
//{
//    return m_values.emplace_back(name);
//}
