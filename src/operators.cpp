#include "operators.h"

std::string toString(OpBinary op)
{
    switch (op)
    {
    case OpBinary::Minus: return "-";
    case OpBinary::Plus: return "+";
    case OpBinary::Multiplication: return "*";
    case OpBinary::Divide: return "/";
    case OpBinary::Equal: return "==";
    case OpBinary::NotEqual: return "!=";
    case OpBinary::LessThan: return "<";
    case OpBinary::GreaterThan: return ">";
    case OpBinary::LessThanOrEqual: return "<=";
    case OpBinary::GreaterThanOrEqual: return ">=";
    }
    return "unknown";
}

std::string toString(OpAssignment op)
{
    switch (op)
    {
    case OpAssignment::Assign: return "=";
    case OpAssignment::MinusAssign: return "-=";
    case OpAssignment::PlusAssign: return "+=";
    case OpAssignment::DivideAssign: return "/=";
    case OpAssignment::MultiplyAssign: return "*=";
    }
    return "unknown";
}

std::optional<OpBinary> toOpBinary(const std::string &string)
{
    if (string == "-")
        return OpBinary::Minus;

    else if (string == "+")
        return OpBinary::Plus;

    else if (string == "/")
        return OpBinary::Divide;

    else if (string == "*")
        return OpBinary::Multiplication;

    else if (string == "==")
        return OpBinary::Equal;

    else if (string == "!=")
        return OpBinary::NotEqual;

    else if (string == "<")
        return OpBinary::LessThan;

    else if (string == ">")
        return OpBinary::GreaterThan;

    else if (string == "<=")
        return OpBinary::LessThanOrEqual;

    else if (string == ">=")
        return OpBinary::GreaterThanOrEqual;

    return std::nullopt;
}

std::optional<OpAssignment> toOpAssignment(const std::string &string)
{
    if (string == "=")
        return OpAssignment::Assign;

    else if (string == "-=")
        return OpAssignment::MinusAssign;

    else if (string == "+=")
        return OpAssignment::PlusAssign;

    else if (string == "/=")
        return OpAssignment::DivideAssign;

    else if (string == "*=")
        return OpAssignment::MultiplyAssign;

    return std::nullopt;
}
