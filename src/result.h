#pragma once

#include "error.h"

#include <variant>

template<typename ValueType>
class Result : public std::variant<ValueType, Error>
{
public:
    using BaseType = std::variant<ValueType, Error>;

    // inherit constructors
    using BaseType::BaseType;

    explicit operator bool() const
    {
        return hasValue();
    }

    const ValueType &operator*() const
    {
        return value();
    }

    ValueType *operator->()
    {
        return &std::get<ValueType>(*this);
    }

    bool hasError() const
    {
        return this->index() == 1;
    }

    bool hasValue() const
    {
        return this->index() == 0;
    }

    const ValueType &value() const
    {
        return std::get<ValueType>(*this);
    }

    ValueType &value()
    {
        return std::get<ValueType>(*this);
    }

    Error error() const
    {
        return std::get<Error>(*this);
    }
};
