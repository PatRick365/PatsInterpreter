#include "operation.h"

#include "value.h"
#include "result.h"

std::string annotateErrMsg(const std::string &errMsg)
{
    return "Illegal binary operation: " + errMsg;
}

namespace Evaluation
{
    Result<Value> add(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [](double lhs, double rhs) -> Result<Value>
            {
                return Value(lhs + rhs);
            },
            [](long lhs, long rhs) -> Result<Value>
            {
                return Value(lhs + rhs);
            },
            [](double lhs, long rhs) -> Result<Value>
            {
                return Value(lhs + double(rhs));
            },
            [](long lhs, double rhs) -> Result<Value>
            {
                return Value(double(lhs) + rhs);
            },
            [](std::string lhs, std::string rhs) -> Result<Value>
            {
                return Value(lhs + rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " + " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }

    Result<Value> substract(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [](double lhs, double rhs) -> Result<Value>
            {
                return Value(lhs - rhs);
            },
            [](long lhs, long rhs) -> Result<Value>
            {
                return Value(lhs - rhs);
            },
            [](double lhs, long rhs) -> Result<Value>
            {
                return Value(lhs - double(rhs));
            },
            [](long lhs, double rhs) -> Result<Value>
            {
                return Value(double(lhs) - rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " + " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }

    Result<Value> multiply(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [](double lhs, double rhs) -> Result<Value>
            {
                return Value(lhs * rhs);
            },
            [](long lhs, long rhs) -> Result<Value>
            {
                return Value(lhs * rhs);
            },
            [](double lhs, long rhs) -> Result<Value>
            {
                return Value(lhs * double(rhs));
            },
            [](long lhs, double rhs) -> Result<Value>
            {
                return Value(double(lhs) * rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " * " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }

    Result<Value> divide(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [&debugInfoOp](double lhs, double rhs) -> Result<Value>
            {
                if (rhs == 0)
                    return Error(debugInfoOp, "division by 0");

                return Value(lhs / rhs);
            },
            [&debugInfoOp](long lhs, long rhs) -> Result<Value>
            {
                if (rhs == 0)
                    return Error(debugInfoOp, "division by 0");

                return Value(lhs / static_cast<double>(rhs));
            },
            [&debugInfoOp](double lhs, long rhs) -> Result<Value>
            {
                if (rhs == 0)
                    return Error(debugInfoOp, "division by 0");

                return Value(lhs / double(rhs));
            },
            [&debugInfoOp](long lhs, double rhs) -> Result<Value>
            {
                if (rhs == 0)
                    return Error(debugInfoOp, "division by 0");

                return Value(double(lhs) / rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " / " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }

    Result<Value> equal(const Value &lhs, const Value &rhs, const DebugInfo &/*debugInfoOp*/)
    {
        return std::visit([](auto &&lhs, auto &&rhs) -> Value
        {
            using T1 = std::decay_t<decltype(lhs)>;
            using T2 = std::decay_t<decltype(rhs)>;

            if constexpr (!std::is_same_v<T1, T2>)
                return Value::makeBool(false);
            else
                return Value::makeBool(lhs == rhs);

        }, lhs.value(), rhs.value());
    }

    Result<Value> notEqual(const Value &lhs, const Value &rhs, const DebugInfo &/*debugInfoOp*/)
    {
        return std::visit([](auto &&lhs, auto &&rhs) -> Value
        {
            using T1 = std::decay_t<decltype(lhs)>;
            using T2 = std::decay_t<decltype(rhs)>;

            if constexpr (!std::is_same_v<T1, T2>)
                return Value::makeBool(true);
            else
                return Value::makeBool(lhs != rhs);

        }, lhs.value(), rhs.value());
    }

    Result<Value> lessThan(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [](double lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs < rhs);
            },
            [](long lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs < rhs);
            },
            [](double lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs < rhs);
            },
            [](long lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs < rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " < " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }

    Result<Value> greaterThan(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [](double lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs > rhs);
            },
            [](long lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs > rhs);
            },
            [](double lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs > rhs);
            },
            [](long lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs > rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " > " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }

    Result<Value> lessOrEqual(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [](double lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs <= rhs);
            },
            [](long lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs <= rhs);
            },
            [](double lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs <= rhs);
            },
            [](long lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs <= rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " < " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }

    Result<Value> greaterOrEqual(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp)
    {
        return std::visit(overloaded
        {
            [](double lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs >= rhs);
            },
            [](long lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs >= rhs);
            },
            [](double lhs, long rhs) -> Result<Value>
            {
                return Value::makeBool(lhs >= rhs);
            },
            [](long lhs, double rhs) -> Result<Value>
            {
                return Value::makeBool(lhs >= rhs);
            },
            [&lhs, &rhs, &debugInfoOp](auto, auto) -> Result<Value>
            {
                return Error(debugInfoOp, annotateErrMsg(lhs.typeStr() + '(' + lhs.toString() + ')' + " > " + rhs.typeStr() + '(' + rhs.toString() + ")"));
            },
        }, lhs.value(), rhs.value());
    }
}

/*Value operator+(const Value &lhs, const Value &rhs)
{
    return Evaluation::add(lhs, rhs);
}

Value operator-(const Value &lhs, const Value &rhs)
{
    return Evaluation::substract(lhs, rhs);
}

Value operator*(const Value &lhs, const Value &rhs)
{
    return Evaluation::multiply(lhs, rhs);
}

Value operator/(const Value &lhs, const Value &rhs)
{
    return Evaluation::divide(lhs, rhs);
}

bool operator==(const Value &lhs, const Value &rhs)
{
    return Evaluation::equal(lhs, rhs);
}

bool operator!=(const Value &lhs, const Value &rhs)
{
    return Evaluation::notEqual(lhs, rhs);
}

bool operator<(const Value &lhs, const Value &rhs)
{
    return Evaluation::lessThan(lhs, rhs);
}

bool operator>(const Value &lhs, const Value &rhs)
{
    return Evaluation::greaterThan(lhs, rhs);
}

bool operator<=(const Value &lhs, const Value &rhs)
{
    return Evaluation::lessOrEqual(lhs, rhs);
}

bool operator>=(const Value &lhs, const Value &rhs)
{
    return Evaluation::greaterOrEqual(lhs, rhs);
}*/

