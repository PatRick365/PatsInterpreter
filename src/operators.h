#pragma once

#include <array>
#include <string>
#include <optional>
#include <unordered_map>

///
/// \brief The OpBinary enum
///
enum class OpBinary
{
    Minus,
    Plus,
    Divide,
    Multiplication,
    Equal,
    NotEqual,
    LessThan,
    GreaterThan,
    LessThanOrEqual,
    GreaterThanOrEqual,
};
static constexpr std::array<OpBinary, 10> listOpBinary
{
    OpBinary::Minus,
    OpBinary::Plus,
    OpBinary::Divide,
    OpBinary::Multiplication,
    OpBinary::Equal,
    OpBinary::NotEqual,
    OpBinary::LessThan,
    OpBinary::GreaterThan,
    OpBinary::LessThanOrEqual,
    OpBinary::GreaterThanOrEqual
};
extern std::string toString(OpBinary op);
extern std::optional<OpBinary> toOpBinary(const std::string &string);


///
/// \brief The OpAssignment enum
///
enum class OpAssignment
{
    Assign,
    MinusAssign,
    PlusAssign,
    DivideAssign,
    MultiplyAssign
};
static constexpr std::array<OpAssignment, 5> listOpAssignment
{
    OpAssignment::Assign,
    OpAssignment::MinusAssign,
    OpAssignment::PlusAssign,
    OpAssignment::DivideAssign,
    OpAssignment::MultiplyAssign
};

static std::unordered_map<OpBinary, int> opBinPrecedence
{
    { OpBinary::Divide, 5 },
    { OpBinary::Multiplication, 5 },
    { OpBinary::Minus, 4 },
    { OpBinary::Plus, 4 },
    { OpBinary::LessThan, 1 },
    { OpBinary::GreaterThan, 1 },
    { OpBinary::LessThanOrEqual, 1 },
    { OpBinary::GreaterThanOrEqual, 1 },
    { OpBinary::Equal, 0 },
    { OpBinary::NotEqual, 0 }
};

static std::unordered_map<OpBinary, int> opBinLeftAssociative
{
    { OpBinary::Divide, true },
    { OpBinary::Multiplication, true },
    { OpBinary::Minus, true },
    { OpBinary::Plus, true },
    { OpBinary::LessThan, true },
    { OpBinary::GreaterThan, true },
    { OpBinary::LessThanOrEqual, true },
    { OpBinary::GreaterThanOrEqual, true },
    { OpBinary::Equal, true },
    { OpBinary::NotEqual, true }
};

extern std::string toString(OpAssignment op);
extern std::optional<OpAssignment> toOpAssignment(const std::string &string);
