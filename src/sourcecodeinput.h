#pragma once

#include "config.h"

#include <string>
#include <fstream>
#include <memory>
#include <vector>
#include <queue>


class ISourceCodeInput
{
public:
    enum State
    {
        HasLine,
        AtEnd,
        Error
    };

    explicit ISourceCodeInput(const Config &config);
    ~ISourceCodeInput() = default;

    virtual bool init() = 0;

    virtual std::string fetchLine(unsigned int indentLevel) = 0;

    bool isAtEnd() const;
    bool isNotAtEnd() const;

    size_t currentLine() const;

    const std::vector<std::string> &sourceLines() const;

    const Config &config() const;   // todo actually use this config

protected:
    std::vector<std::string> m_sourceLines;

    bool m_isAtEnd;
    size_t m_line = 0;

private:
    Config m_config;
};

class CinSourceCodeInput : public ISourceCodeInput
{
public:
    CinSourceCodeInput();

    virtual bool init() override
    {
        return true;
    }

    std::string fetchLine(unsigned int indentLevel) override;
};

class FileSourceCodeInput : public ISourceCodeInput
{
public:
    explicit FileSourceCodeInput(const std::string &path);

    virtual bool init() override
    {
        return openFile(m_path);
    }

    bool openFile(const std::string &path);

    std::string fetchLine(unsigned int) override;

private:
    const std::string m_path;
    std::ifstream m_file;
};

class StringSourceCodeInput : public ISourceCodeInput
{
public:
    StringSourceCodeInput(const Config &config, const std::vector<std::string> &code);

    virtual bool init() override
    {
        return true;
    }

    std::string fetchLine(unsigned int) override;

private:
    std::vector<std::string> m_code;
};
