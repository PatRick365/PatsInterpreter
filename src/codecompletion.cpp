#include "codecompletion.h"

#include <readline/readline.h>
#include <iostream>

char *completionGenerator(const char *text, int state)
{
    // This function is called with state=0 the first time; subsequent calls are
    // with a nonzero state. state=0 can be used to perform one-time
    // initialization for this completion session.
    static std::vector<std::string> matches;
    static size_t match_index = 0;

    if (state == 0)
    {
        // During initialization, compute the actual matches for 'text' and keep
        // them in a static vector.
        matches.clear();
        match_index = 0;

        // Collect a vector of matches: vocabulary words that begin with text.
        std::string textstr(text);
        for (auto word : CodeCompletion::get().vocabulary())
        {
            if (word.size() >= textstr.size() && word.compare(0, textstr.size(), textstr) == 0)
            {
                matches.push_back(word);
            }
        }
    }

    if (match_index >= matches.size())
    {
        // We return nullptr to notify the caller no more matches are available.
        return nullptr;
    }
    else
    {
        // Return a malloc'd char* for the match. The caller frees it.
        return strdup(matches[match_index++].c_str());
    }
}

char **tabMatch(const char *text)
{
    size_t lenText = strlen(text);

    char **orderedIds = (char **)malloc(2 * sizeof(char *));
    orderedIds[0] = (char*)malloc((lenText + 5)* sizeof(char)); /*+1 for '\0' character */

    for (size_t i = 0; i < lenText; ++i)
        orderedIds[0][i] = text[i];

    for (size_t i = 0; i < 4; ++i)
        orderedIds[0][i + lenText] = ' ';

    orderedIds[0][lenText + 4] = '\0';

/*
    for (size_t i = 0; i < 3; ++i)
        orderedIds[0][i] = ' ';

    for (size_t i = 3; i < lenText + 3; ++i)
        orderedIds[0][i] = text[i - 3];

    orderedIds[0][lenText + 3] = '\0';
*/

    orderedIds[1] = nullptr;

    return orderedIds;
}

char **completer(const char *text, int, int)
{
    // Don't do filename completion even if our generator finds no matches.
    ::rl_attempted_completion_over = 1;

    // Note: returning nullptr here will make readline use the default filename completer.

    char **matches = ::rl_completion_matches(text, completionGenerator);

    bool isTab = (strcmp(text, "\t") != 0);

    // add text to match

    // hack to make tab input possible alongside tab completion
    if (isTab && matches == nullptr)
    {
        for (char *str = *matches; str; ++str)
            free(str);

//        for (size_t i = 0; matches[i] != nullptr; ++i)
//            free(matches[i]);

        return tabMatch(text);
    }

    char *str = *matches;
    size_t len = 0;

    while (str != nullptr)
    {
        str = matches[++len];
    }

    if (isTab && (len == 0 || len - 1 == CodeCompletion::get().vocabulary().size()))
    {
        for (size_t i = 0; matches[i] != nullptr; ++i)
            free(matches[i]);

        return tabMatch(text);
    }

    return matches;
}

void CodeCompletion::registerWord(const std::string &word)
{
    m_vocabulary.emplace_back(word);
}

void CodeCompletion::unRegisterWord(const std::string &word)
{
    auto it = std::find(m_vocabulary.cbegin(), m_vocabulary.cend(), word);

    if (it != m_vocabulary.cend())
        m_vocabulary.erase(it);
}

const std::vector<std::string> &CodeCompletion::vocabulary() const
{
    return m_vocabulary;
}
