#include "logging.h"


std::string toString(LogId logId)
{
    switch (logId)
    {
    case LogId::AstExecution:
        return "AstExecution";
    case LogId::AstCreation:
        return "AstCreation";
    case LogId::Parser:
        return "Parser";
    case LogId::Lexer:
        return "Lexer";
    case LogId::SourceInput:
        return "SourceInput";
    case LogId::Stack:
        return "Stack";
    }
    return "LogId::Unknown";
}

AutoLogging::AutoLogging(const std::unordered_set<LogId> &logIds)
    : m_logIdsBefore(Logging::get().activeLogIds())
{
    Logging::get().setOn(logIds);
}

AutoLogging::~AutoLogging()
{
    Logging::get().setOn(m_logIdsBefore);
}

Logging &Logging::get()
{
    static Logging instance;
    return instance;
}

bool Logging::isOn(LogId logId) const
{
    return (m_loggingOn.find(logId) != m_loggingOn.end());
}

void Logging::setOn(LogId logId, bool on)
{
    if (on)
    {
        m_loggingOn.insert(logId);
    }
    else
    {
        m_loggingOn.erase(logId);
    }
}

const std::unordered_set<LogId> &Logging::activeLogIds() const
{
    return m_loggingOn;
}

void Logging::setOn(const std::unordered_set<LogId> &logIds)
{
    m_loggingOn = logIds;
}

void Logging::clear()
{
    m_loggingOn.clear();
}

