#pragma once

#include "value.h"
#include "token.h"
#include "error.h"
#include "result.h"

#include <iostream>
#include <filesystem>
#include <vector>

#include <string>

// could currently be a namespace
class Lexer
{
public:
    Result<std::vector<Token>> fetchLine(size_t row, const std::string &line);

private:
    Result<std::vector<TokenString>> split(size_t row, const std::string &line);
    Result<std::vector<Token>> indexLine(size_t row, const std::vector<TokenString> &line);

};

