#include "token.h"

std::string toString(TokenType type)
{
    switch (type)
    {
    case TokenType::Nix: return "Nix";
    case TokenType::Empty: return "Empty";
    case TokenType::Indent: return "Indent";
//    case TokenType::ScopeOpen: return "ScopeOpen";
//    case TokenType::ScopeClose: return "ScopeClose";
    case TokenType::Name: return "Name";
    case TokenType::Value: return "Value";
    case TokenType::OpBinary: return "OpBinary";
    case TokenType::OpAssignment: return "OpAssignment";
    case TokenType::Keyword: return "Keyword";
    case TokenType::Function: return "Function";
    case TokenType::ParenthesesOpen: return "ParenthesesOpen";
    case TokenType::ParenthesesClose: return "ParenthesesClose";
    case TokenType::Comma: return "Comma";
    case TokenType::If: return "If";
    case TokenType::Else: return "Else";
    case TokenType::While: return "While";
    }

    return "Unknown";
}

TokenString::TokenString(const std::string &string, const DebugInfo &debugInfo)
    : str(string),
      debugInfo(debugInfo)
{}

const std::vector<TokenType> Token::listStatementTokenTypes =
{
    TokenType::OpAssignment,
    TokenType::If,
    TokenType::Else,
//    TokenType::ScopeOpen,
//    TokenType::ScopeClose,
    TokenType::While,
    TokenType::Function,
};

//const std::array<TokenType, 17> Token::listAllTokenTypes
//{
//    TokenType::Nix,
//    TokenType::Empty,
//    TokenType::ScopeOpen,
//    TokenType::ScopeClose,
//    TokenType::Indent,
//    TokenType::Name,
//    TokenType::Value,
//    TokenType::OpBinary,
//    TokenType::OpAssignment,
//    TokenType::Keyword,
//    TokenType::Function,
//    TokenType::BracketOpen,
//    TokenType::BracketClose,
//    TokenType::Comma,
//    TokenType::If,
//    TokenType::Else,
//    TokenType::While
//};

Token::Token()
    : m_type(TokenType::Nix)
{}

Token::Token(const TokenString &tokenString, TokenType type)
    : m_tokenString(tokenString),
      m_type(type),
      m_opBinary(static_cast<OpBinary>(-1)),
      m_oAssignment(static_cast<OpAssignment>(-1))
{}

Token::Token(const TokenString &tokenString, const Value &value)
    : m_tokenString(tokenString),
      m_type(TokenType::Value),
      m_value(value),
      m_opBinary(static_cast<OpBinary>(-1)),
      m_oAssignment(static_cast<OpAssignment>(-1))
{}

Token::Token(const TokenString &tokenString, OpBinary op)
    : m_tokenString(tokenString),
      m_type(TokenType::OpBinary),
      m_opBinary(op),
      m_oAssignment(static_cast<OpAssignment>(-1))

{}

Token::Token(const TokenString &tokenString, OpAssignment op)
    : m_tokenString(tokenString),
      m_type(TokenType::OpAssignment),
      m_opBinary(static_cast<OpBinary>(-1)),
      m_oAssignment(op)

{}

std::string Token::toString() const
{
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

const DebugInfo &Token::debugInfo() const
{
    return m_tokenString.debugInfo;
}

std::ostream &operator<<(std::ostream &os, const Token &expression)
{
    os << expression.m_tokenString.debugInfo.row << ":" << expression.m_tokenString.debugInfo.col << " - \"" << expression.m_tokenString.str << "\" - " << toString(expression.m_type);
    if (expression.m_type == TokenType::Value)
        os << " - " << expression.m_value.typeStr() << ": " << expression.m_value;

    return os;
}

std::string toString(const std::vector<Token> &tokens)
{
    std::stringstream ss;

    for (const auto &token : tokens)
        ss << token.toString() << " ";

    return ss.str();
}

namespace TokenHelper
{
TokenIterator find(TokenIterator itBegin, TokenIterator itEnd, const std::vector<TokenType> &tokenTypeList)
{
    for (TokenType tokenType : tokenTypeList)
    {
        auto it = std::find_if(itBegin, itEnd, [tokenType](const Token &token)
        {
            return (token.m_type == tokenType);
        });

        if (it != itEnd)
            return it;
    }
    return itEnd;
}

TokenIterator find(TokenIterator itBegin, TokenIterator itEnd, TokenType tokenType)
{
    return std::find_if(itBegin, itEnd, [tokenType](const Token &token)
    {
        return (token.m_type == tokenType);
    });
}

TokenIterator findMatchingEndParenthesis(TokenIterator itBegin, TokenIterator itEnd)
{
    if (itBegin->m_type != TokenType::ParenthesesOpen)
        throw std::runtime_error("findMatchingEndParenthesis: itBegin not of type BracketOpen");

    int openParentheses = 0;

    for (; itBegin != itEnd; ++itBegin)
    {
        if (itBegin->m_type == TokenType::ParenthesesOpen)
            ++openParentheses;
        else if (itBegin->m_type == TokenType::ParenthesesClose)
            --openParentheses;

        if (openParentheses == 0)
            return itBegin;
    }
    return itEnd;
}

size_t getIndentLevel(TokenIterator itBegin, TokenIterator itEnd)
{
    size_t indentLevel = 0;

    for ( ; itBegin != itEnd; ++itBegin)
    {
        if (itBegin->m_type == TokenType::Indent)
            ++indentLevel;
        else
            break;
    }
    return indentLevel;
}

bool canFollowItself(TokenType type)
{
    switch (type)
    {
    // yes
    case TokenType::Indent:
//    case TokenType::ScopeOpen:
//    case TokenType::ScopeClose:
    case TokenType::ParenthesesOpen:
    case TokenType::ParenthesesClose:
        return true;

        // no
    case TokenType::Nix:
    case TokenType::Empty:
    case TokenType::Name:
    case TokenType::Value:
    case TokenType::OpBinary:
    case TokenType::OpAssignment:
    case TokenType::Keyword:
    case TokenType::Function:
    case TokenType::If:
    case TokenType::Else:
    case TokenType::While:
    case TokenType::Comma:
        return false;
    }

    return false;
}

int getPrecedence(Token token)
{
    if (token.m_type == TokenType::OpBinary)
        return opBinPrecedence[token.m_opBinary];

    if (token.m_type == TokenType::Function)
        return 8;

    if (token.m_type == TokenType::ParenthesesClose || token.m_type == TokenType::ParenthesesOpen)
        return 0;   // todo: whatever, but seems to work

    throw std::runtime_error("getPrecedence: unhandled token '" + token.m_tokenString.str + "' of type " + toString(token.m_type));
}

bool getIsLeftAssociative(Token token)
{
    if (token.m_type == TokenType::OpBinary)
        return opBinLeftAssociative[token.m_opBinary];

    if (token.m_type == TokenType::Function)
        return true;

    throw std::runtime_error("getIsLeftAssociative: unhandled token '" + token.m_tokenString.str + "' of type " + toString(token.m_type));
}

}
