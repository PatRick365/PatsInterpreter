#include "config.h"
#include "sourcecodeinput.h"
#include "logging.h"
#include "codecompletion.h"

#include <iostream>
#include <filesystem>

#include <readline/readline.h>
#include <readline/history.h>


template<typename T>
struct MallocDeleter
{
    void operator()(T *p) { free(p); }
};

ISourceCodeInput::ISourceCodeInput(const Config &config)
    : m_isAtEnd(false),
      m_config(config)
{}

bool ISourceCodeInput::isAtEnd() const
{
    return m_isAtEnd;
}

bool ISourceCodeInput::isNotAtEnd() const
{
    return !m_isAtEnd;
}

size_t ISourceCodeInput::currentLine() const
{
    return m_sourceLines.size();
}

const std::vector<std::string> &ISourceCodeInput::sourceLines() const
{
    return m_sourceLines;
}


CinSourceCodeInput::CinSourceCodeInput()
    : ISourceCodeInput(Config().printStandAloneExpressions())
{
    std::cout << "Welcome to Pint! You can exit by pressing Ctrl+C at any time...\n";

    //::rl_bind_key('\t', rl_insert);

    ::rl_attempted_completion_function = completer;
    //::rl_completion_append_character = 'X';
}

std::string CinSourceCodeInput::fetchLine(unsigned int indentLevel)
{
    std::string promt = (indentLevel > 0) ? "... " : ">>> ";

    auto buf = std::unique_ptr<char, MallocDeleter<char>>{ ::readline(promt.c_str()) };

    if (buf == nullptr)
    {
        std::cout << "nullptr";
        return {};
    }

    if (strlen(buf.get()) > 0)
    {
        add_history(buf.get());
    }

    std::string line(buf.get());

    m_sourceLines.push_back(line);

    LOG_PRETTY(LogId::SourceInput, "got input: " + line);

    return line;
}


FileSourceCodeInput::FileSourceCodeInput(const std::string &path)
    : ISourceCodeInput(Config().exitOnError()),
      m_path(path)
{}

bool FileSourceCodeInput::openFile(const std::string &path)
{
    if (!std::filesystem::exists(path))
    {
        std::cout << "file " << path << " does not exist..\n";
        return false;
    }

    if (!std::filesystem::is_regular_file(path))
    {
        std::cout << path << " is not a regular file..\n";
        return false;
    }

    m_file = std::ifstream(path);

    if (!m_file.is_open())
    {
        std::cout << "file " << path << " not open..\n";
        return false;
    }

    return true;
}

std::string FileSourceCodeInput::fetchLine(unsigned int)
{
    ++m_line;

    std::string line;
    if (std::getline(m_file, line))
    {
        LOG_PRETTY(LogId::SourceInput, "got input: " + line);

        m_sourceLines.push_back(line);

        return line;
    }

    m_isAtEnd = true;
    return {};
}

StringSourceCodeInput::StringSourceCodeInput(const Config &config, const std::vector<std::string> &code)
    : ISourceCodeInput(config),
      m_code(code)
{}

std::string StringSourceCodeInput::fetchLine(unsigned int)
{
    ++m_line;

    if (m_line <= m_code.size())
    {
        std::string line = std::move(m_code[m_line - 1]);

        LOG_PRETTY(LogId::SourceInput, "got input: " + line);

        m_sourceLines.push_back(line);

        return line;
    }

    m_isAtEnd = true;
    return {};
}
