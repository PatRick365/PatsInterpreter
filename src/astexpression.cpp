#include "astexpression.h"

#include "ast.h"


Expression::Expression(const DebugInfo &debugInfo)
    : debugInfo(debugInfo)
{}

ExpOpBinary::Ptr ExpOpBinary::make(Expression::Ptr lhv, OpBinary op, Expression::Ptr rhv, const DebugInfo &debugInfo)
{
    return std::make_unique<ExpOpBinary>(std::move(lhv), op, std::move(rhv), debugInfo);
}

bool ExpOpBinary::acceptExpression(Visitor *vistior)
{
    return vistior->visit(this);
}

ExpOpBinary::ExpOpBinary(Expression::Ptr lhv, OpBinary op, Expression::Ptr rhv, const DebugInfo &debugInfo)
    : Expression(debugInfo),
      op(op),
      lhs(std::move(lhv)),
      rhs(std::move(rhv))
{}

ExpValue::Ptr ExpValue::make(const Value &value, const DebugInfo &debugInfo)
{
    return std::make_unique<ExpValue>(value, debugInfo);
}

ExpValue::ExpValue(const DebugInfo &debugInfo)
    : Expression(debugInfo)
{}

ExpValue::ExpValue(const Value &value, const DebugInfo &debugInfo)
    : Expression(debugInfo),
      value(value)
{}

bool ExpValue::acceptExpression(Visitor *vistior)
{
    return vistior->visit(this);
}

ExpLValue::Ptr ExpLValue::make(const std::string &name, const DebugInfo &debugInfo)
{
    return std::make_unique<ExpLValue>(name, debugInfo);
}

ExpLValue::ExpLValue()
    : ExpValue(DebugInfo())
{}

ExpLValue::ExpLValue(const std::string &name, const DebugInfo &debugInfo)
    : ExpValue(debugInfo),
      name(name)
{}

bool ExpLValue::acceptExpression(Visitor *vistior)
{
    return vistior->visit(this);
}

ExpFunction::Ptr ExpFunction::make(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo)
{
    return std::make_unique<ExpFunction>(name, std::move(args), debugInfo);
}

ExpFunction::ExpFunction(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo)
    : Expression(debugInfo),
      name(name),
      args(std::move(args))
{}

bool ExpFunction::acceptExpression(Visitor *vistior)
{
    return vistior->visit(this);
}
