#include "config.h"

Config::Config()
    : m_printStandAloneExpressions(false),
      m_exitOnError(false)
{}

Config &Config::printStandAloneExpressions() { m_printStandAloneExpressions = true; return *this; }
Config &Config::exitOnError() { m_exitOnError = true; return *this; }
