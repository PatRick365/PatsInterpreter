#pragma once

#include "astexpression.h"
#include "operators.h"

#include <memory>
#include <vector>
#include <string>


class Visitor;

class Statement
{
public:
    using Ptr = std::unique_ptr<Statement>;

    explicit Statement(const DebugInfo &debugInfo);

    virtual ~Statement() = default;

    [[nodiscard]] virtual bool acceptStatement(Visitor *vistior) = 0;

    DebugInfo debugInfo;
};

class StatNoOp : public Statement
{
public:
    using Ptr = std::unique_ptr<StatNoOp>;

    StatNoOp()
        : Statement(DebugInfo())
    {}

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;
};

class StatementBlock : public Statement
{
public:
    using Ptr = std::unique_ptr<StatementBlock>;

    explicit StatementBlock(const DebugInfo &debugInfo);

    std::vector<Statement::Ptr> statements;

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;
};

class StatAssignment : public Statement
{
public:
    using Ptr = std::unique_ptr<StatAssignment>;

    static StatAssignment::Ptr make(ExpLValue::Ptr lhv, OpAssignment op, Expression::Ptr rhv, const DebugInfo &debugInfo);

    explicit StatAssignment(ExpLValue::Ptr lhv, OpAssignment op, Expression::Ptr rhv, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;

    std::unique_ptr<ExpLValue> lValue;
    OpAssignment op;
    Expression::Ptr rValue;
};

class StatIfThenElse : public Statement
{
public:
    using Ptr = std::unique_ptr<StatIfThenElse>;

    static StatIfThenElse::Ptr make(Expression::Ptr condition, Statement::Ptr ifBranch, Statement::Ptr elseBranch, const DebugInfo &debugInfo);

    explicit StatIfThenElse(const DebugInfo &debugInfo);
    StatIfThenElse(Expression::Ptr condition, Statement::Ptr ifBranch, Statement::Ptr elseBranch, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;

    Expression::Ptr condition;
    Statement::Ptr ifBranch;
    Statement::Ptr elseBranch;
};

class StatWhile : public Statement
{
public:
    using Ptr = std::unique_ptr<StatWhile>;

    static StatWhile::Ptr make(Expression::Ptr condition, Statement::Ptr body, const DebugInfo &debugInfo);

    explicit StatWhile(Expression::Ptr condition, Statement::Ptr body, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;

    Expression::Ptr condition;
    Statement::Ptr body;
};

class StatFor : public Statement
{
public:
    using Ptr = std::unique_ptr<StatFor>;

    explicit StatFor(StatAssignment::Ptr initStatement, Expression::Ptr condition,
                     Statement::Ptr iterationExpression, Statement::Ptr body, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;

    StatAssignment::Ptr initStatement;
    Expression::Ptr condition;
    Statement::Ptr iterationExpression;
    Statement::Ptr body;
};

class StatPrint : public Statement
{
public:
    using Ptr = std::unique_ptr<StatPrint>;

    static StatPrint::Ptr make(Expression::Ptr expression, const DebugInfo &debugInfo);

    explicit StatPrint(Expression::Ptr expression, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;

    Expression::Ptr expression;
};

class StatFunction : public Statement
{
public:
    using Ptr = std::unique_ptr<StatFunction>;

    static StatFunction::Ptr make(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo);

    StatFunction(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo);

    [[nodiscard]] bool acceptStatement(Visitor *vistior) override;

    std::string name;
    std::vector<Expression::Ptr> args;
};
