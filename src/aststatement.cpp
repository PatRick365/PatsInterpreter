#include "aststatement.h"

#include "ast.h"


Statement::Statement(const DebugInfo &debugInfo)
    : debugInfo(debugInfo)
{}

StatIfThenElse::Ptr StatIfThenElse::make(Expression::Ptr condition, Statement::Ptr ifBranch, Statement::Ptr elseBranch, const DebugInfo &debugInfo)
{
    return std::make_unique<StatIfThenElse>(std::move(condition), std::move(ifBranch), std::move(elseBranch), debugInfo);
}

StatIfThenElse::StatIfThenElse(const DebugInfo &debugInfo)
    : Statement(debugInfo)
{}

StatIfThenElse::StatIfThenElse(Expression::Ptr condition, Statement::Ptr ifBranch, Statement::Ptr elseBranch, const DebugInfo &debugInfo)
    : Statement(debugInfo),
      condition(std::move(condition)),
      ifBranch(std::move(ifBranch)),
      elseBranch(std::move(elseBranch))
{}

bool StatNoOp::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}

bool StatIfThenElse::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}

StatementBlock::StatementBlock(const DebugInfo &debugInfo)
    : Statement(debugInfo)
{}

bool StatementBlock::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}

StatAssignment::Ptr StatAssignment::make(ExpLValue::Ptr lhv, OpAssignment op, Expression::Ptr rhv, const DebugInfo &debugInfo)
{
    return std::make_unique<StatAssignment>(std::move(lhv), op, std::move(rhv), debugInfo);
}

StatAssignment::StatAssignment(ExpLValue::Ptr lhv, OpAssignment op, Expression::Ptr rhv, const DebugInfo &debugInfo)
    : Statement(debugInfo),
      lValue(std::move(lhv)),
      op(op),
      rValue(std::move(rhv))
{}

bool StatAssignment::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}

StatWhile::StatWhile(Expression::Ptr condition, Statement::Ptr body, const DebugInfo &debugInfo)
    : Statement(debugInfo),
      condition(std::move(condition)),
      body(std::move(body))
{}

StatWhile::Ptr StatWhile::make(Expression::Ptr condition, Statement::Ptr body, const DebugInfo &debugInfo)
{
    return std::make_unique<StatWhile>(std::move(condition), std::move(body), debugInfo);
}

bool StatWhile::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}

StatFor::StatFor(StatAssignment::Ptr initStatement, Expression::Ptr condition,
                 Statement::Ptr iterationExpression, Statement::Ptr body, const DebugInfo &debugInfo)
    : Statement(debugInfo),
      initStatement(std::move(initStatement)),
      condition(std::move(condition)),
      iterationExpression(std::move(iterationExpression)),
      body(std::move(body))
{}

bool StatFor::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}

StatPrint::Ptr StatPrint::make(Expression::Ptr expression, const DebugInfo &debugInfo)
{
    return std::make_unique<StatPrint>(std::move(expression), debugInfo);
}

StatPrint::StatPrint(Expression::Ptr expression, const DebugInfo &debugInfo)
    : Statement(debugInfo),
      expression(std::move(expression))
{}

bool StatPrint::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}

StatFunction::Ptr StatFunction::make(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo)
{
    return std::make_unique<StatFunction>(name, std::move(args), debugInfo);
}

StatFunction::StatFunction(const std::string &name, std::vector<Expression::Ptr> args, const DebugInfo &debugInfo)
    : Statement(debugInfo),
      name(name),
      args(std::move(args))
{}

bool StatFunction::acceptStatement(Visitor *vistior)
{
    return vistior->visit(this);
}
