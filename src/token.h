#pragma once

#include "value.h"
#include "operators.h"

#include <string>
#include <unordered_map>
#include <iostream>
#include <vector>
#include <cassert>


enum class TokenType
{
    Nix = 1,
    Empty,
    Indent,
//    ScopeOpen,
//    ScopeClose,
    Name,
    Value,
    OpBinary,
    OpAssignment,
    Keyword,
    Function,
    ParenthesesOpen,
    ParenthesesClose,
    Comma,
    If,
    Else,
    While
};
extern std::string toString(TokenType type);

struct TokenString
{
    TokenString() = default;
    TokenString(const std::string &string, const DebugInfo &debugInfo);

    std::string str;
    DebugInfo debugInfo;
};

class Token
{
public:
    static const std::vector<TokenType> listStatementTokenTypes;

    Token();
    Token(const TokenString &tokenString, TokenType type);
    Token(const TokenString &tokenString, const Value &value);
    Token(const TokenString &tokenString, OpBinary op);
    Token(const TokenString &tokenString, OpAssignment op);

    friend std::ostream& operator<<(std::ostream &os, const Token &expression);

    std::string toString() const;

    const DebugInfo &debugInfo() const;

    TokenString m_tokenString;
    TokenType m_type;
    Value m_value;
    OpBinary m_opBinary;
    OpAssignment m_oAssignment;
};

//class TokenList2 : public std::vector<Token>
//{
//public:
//    TokenList2::const_iterator findStatement()
//    {
//        for (TokenType tokenType : Token::listStatementTokenTypes)
//        {
//            auto it = std::find_if(cbegin(), cend(), [tokenType](const Token &token)
//            {
//                return (token.m_type == tokenType);
//            });

//            if (it != cend())
//                return it;
//        }
//        return cend();
//    }
//};

std::string toString(const std::vector<Token> &tokens);

using TokenIterator = std::vector<Token>::const_iterator;

namespace TokenHelper
{
TokenIterator find(TokenIterator itBegin, TokenIterator itEnd, const std::vector<TokenType> &tokenTypeList);
TokenIterator find(TokenIterator itBegin, TokenIterator itEnd, TokenType tokenType);
TokenIterator findMatchingEndParenthesis(TokenIterator itBegin, TokenIterator itEnd);
size_t getIndentLevel(TokenIterator itBegin, TokenIterator itEnd);
bool canFollowItself(TokenType type);
int getPrecedence(Token token);
bool getIsLeftAssociative(Token token);
}



