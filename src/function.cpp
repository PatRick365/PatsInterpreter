#include "function.h"
#include "operation.h"
#include "codecompletion.h"

#include <algorithm>


namespace Fn
{

Result<Value> print(const std::vector<Value> &args, const DebugInfo &)
{
    std::cout << args.front();
    return Value::null;
}

Result<Value> println(const std::vector<Value> &args, const DebugInfo &)
{
    std::cout << args.front() << "\n";
    return Value::null;
}

Result<Value> abs(const std::vector<Value> &args, const DebugInfo &debugInfo)
{
    const Value &value = args.front();

    if (!value.isNumber())
        return Error(debugInfo, "function expects number as argument");

    // todo: do these need checks? or take a lower level aproach
    if (Evaluation::lessThan(value, Value(0), debugInfo).value().isBoolTrue())
    {
        return Evaluation::multiply(value, Value(-1), debugInfo).value();
    }

    return value;
}

Result<Value> min(const std::vector<Value> &args, const DebugInfo &debugInfo)
{
    const Value &valueLhs = args.front();
    const Value &valueRhs = args.back();

    if (!valueLhs.isNumber() || !valueRhs.isNumber())
        return Error(debugInfo, "function expects 2 numbers as arguments");

    if (Evaluation::lessThan(valueLhs, valueRhs, debugInfo).value().isBoolTrue())
        return valueLhs;

    return valueRhs;
}

Result<Value> max(const std::vector<Value> &args, const DebugInfo &debugInfo)
{
    const Value &valueLhs = args.front();
    const Value &valueRhs = args.back();

    if (!valueLhs.isNumber() || !valueRhs.isNumber())
        return Error(debugInfo, "function expects 2 numbers as arguments");

    if (Evaluation::greaterThan(valueLhs, valueRhs, debugInfo).value().isBoolTrue())
        return valueLhs;

    return valueRhs;
}

Result<Value> getline(const std::vector<Value> &/*args*/, const DebugInfo &/*debugInfo*/)
{
    std::string input;
    //std::cout << "What is your name? ";
    std::getline(std::cin, input);
    //std::cout << "Hello " << name << ", nice to meet you.\n";

    return Value(input);
}

}

std::vector<Function>::iterator findFunction(const std::string &name)
{
    return std::find_if(Function::list.begin(), Function::list.end(), [&name](const Function &function)
    {
        return (function.name == name);
    });
}

std::vector<Function> Function::list
{
    // void functions
    Function("print", 1, false, &Fn::print),
    Function("println", 1, false, &Fn::println),

    // non void functions
    Function("abs", 1, true, &Fn::abs),
    Function("min", 2, true, &Fn::min),
    Function("max", 2, true, &Fn::max),
    Function("getline", 0, true, &Fn::getline)

};

Function::Function(const std::string &name, size_t argNum, bool returnVal, const Function::FuncType &func)
    : name(name),
      argNum(argNum),
      returnVal(returnVal),
      func(func)
{
    CodeCompletion::get().registerWord(name + "(");
}

bool Function::exists(const std::string &name)
{
    return findFunction(name) != Function::list.end();
}

std::optional<Function> Function::getFunction(const std::string &name)
{
    auto it = findFunction(name);

    if (it != Function::list.end())
    {
        return *it;
    }
    return std::nullopt;
}
