#pragma once

#include "logging.h"

#include <optional>
#include <unordered_set>

namespace CmdParse
{

struct Options
{
    std::optional<std::string> file;
    std::unordered_set<LogId> logIds;
};

std::optional<Options> parsePintConfig(int argc, char *argv[]);

}
