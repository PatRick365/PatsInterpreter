#pragma once

#include "config.h"
#include "astexpression.h"
#include "aststatement.h"
#include "stack.h"
#include "error.h"


#include <memory>

//#define IF_FALSE_RETURN_FALSE(x) do { if (!x) return false;  } while(false)

// seems like this visitor does not need to be polymorphic
class Visitor
{
public:
    explicit Visitor(const Config &config);

    [[nodiscard]] virtual bool visit(ExpOpBinary *bop) = 0;
    [[nodiscard]] virtual bool visit(ExpValue *valueExp) = 0;
    [[nodiscard]] virtual bool visit(ExpLValue *lVal) = 0;
    [[nodiscard]] virtual bool visit(ExpFunction *function) = 0;

    [[nodiscard]] virtual bool visit(Statement *statement) = 0;
    [[nodiscard]] virtual bool visit(StatNoOp *statNoOp) = 0;
    [[nodiscard]] virtual bool visit(StatementBlock *statementBlock) = 0;
    [[nodiscard]] virtual bool visit(StatIfThenElse *ite) = 0;
    [[nodiscard]] virtual bool visit(StatAssignment *assignment) = 0;
    [[nodiscard]] virtual bool visit(StatWhile *statWhile) = 0;
    [[nodiscard]] virtual bool visit(StatFor *statFor) = 0;
    [[nodiscard]] virtual bool visit(StatPrint *statPrint) = 0;
    [[nodiscard]] virtual bool visit(StatFunction *statFunction) = 0;

protected:
    Config m_config;
};

class InterpreterVisitor : public Visitor
{
public:
    InterpreterVisitor(const Config &config, Stack &stack);

    [[nodiscard]] bool visit(ExpOpBinary *bop) override;
    [[nodiscard]] bool visit(ExpValue *valueExp) override;
    [[nodiscard]] bool visit(ExpLValue *lVal) override;
    [[nodiscard]] bool visit(ExpFunction *function) override;

    [[nodiscard]] bool visit(Statement *statement) override;
    [[nodiscard]] bool visit(StatNoOp *statNoOp) override;
    [[nodiscard]] bool visit(StatementBlock *statementBlock) override;
    [[nodiscard]] bool visit(StatIfThenElse *ifThenElse) override;
    [[nodiscard]] bool visit(StatAssignment *assignment) override;
    [[nodiscard]] bool visit(StatWhile *statWhile) override;
    [[nodiscard]] bool visit(StatFor *statFor) override;
    [[nodiscard]] bool visit(StatPrint *statPrint) override;
    [[nodiscard]] bool visit(StatFunction *statFunction) override;

    void reset();

    Stack &m_stack;

    // Field used to save resulting type of a visit
    Value m_resultVal;

    std::optional<Error> m_error;

};

//class Ast
//{
//public:
//    Ast()
//        : root(std::make_unique<StatementBlock>(DebugInfo()))
//    {}
//
//    StatementBlock *start()
//    {
//        return root.get();
//    }

//    Statement::Ptr root;
//};
