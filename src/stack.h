#pragma once

#include "value.h"
#include "astexpression.h"

#include <unordered_map>
#include <vector>
#include <string>

class Stack;
class AutoPopStack
{
public:
    AutoPopStack(Stack &stack);
    ~AutoPopStack();

private:
    Stack &m_stack;
};

class AutoPushPopStack
{
public:
    AutoPushPopStack(Stack &stack);
    ~AutoPushPopStack();

private:
    Stack &m_stack;
};

struct Scope
{
    Scope() = default;
    Scope(const Scope &other);
    ~Scope();

    std::unordered_map<std::string, ExpLValue> namedValues;
};

class Stack
{
public:
    Stack();
    //Stack(Stack &other);

    //Stack &operator=(Stack &other);

    void push();
    void pop();

    void dump() const;

    ExpLValue &findOrCreateStackValue(const std::string &name, const DebugInfo &debugInfo);
    ExpLValue *findStackValue(const std::string &name);
    bool hasStackValue(const std::string &name);
    ExpLValue &createStackValue(const std::string &name, const DebugInfo &debugInfo);

    AutoPopStack hookAutoPop()
    {
        return AutoPopStack(*this);
    }

    size_t depth() const { return m_scopes.size(); }

    size_t valueCount() const;

    std::vector<Scope> m_scopes;
};

//class Stack2
//{
//public:
//    Stack2();

//    // callee must remember its pos at stack ...
//    // lookup scope would depend on that if functions would be implemented

//    std::vector<ExpLValue> m_values;

//    ExpLValue &findOrCreateStackValue(const std::string &name);
//    ExpLValue *findStackValue(const std::string &name);
//    ExpLValue &createStackValue(const std::string &name);

//};


/*
Welcome to Pint! You can exit by pressing Ctrl+C at any time...
>>> xyz = 3
>>> xyz =3
deleting xyz
>>> if (true)
...     b=3
...
deleting xyz
deleting b
deleting xyz
>>>
abs(      print(    println(  test2(
>>> x
*/
