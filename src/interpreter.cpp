#include "interpreter.h"
#include "logging.h"
#include "function.h"

#include <stack>
#include <cassert>

#define forever for (;;)

void printError(const std::string &line, const Error &err)
{
    auto col = std::max(err.m_col, 1UL);
    auto strRow = std::to_string(err.m_row);

    std::cerr << err.m_row << ":" << col << " Error: " << err.msg() << '\n';
    std::cerr << "  " << strRow << " | " << line << '\n';
    std::cerr << "  " << std::string(strRow.size(), ' ') << " | " << std::string(col - 1, ' ') << '^' << '\n';
}

Interpreter::Interpreter(const Config &config, ISourceCodeInput &sourceInput)
    : config(config),
      interpreterVisitor(config, m_stack),
      m_sourceInput(sourceInput)
{}

int Interpreter::run()
{
    if (!m_sourceInput.init())
        return 1;

    while (!m_sourceInput.isAtEnd() || !m_rejectedTokens.empty())
    {
        //lastError.reset();

        m_atBlockBegin = true;
        Result<Statement::Ptr> result = fetchStatement();

        Stack stackBefore = m_stack;  // thats expensive :-/

        if (result.hasError())
        {
            lastError = result.error();
            printError(m_sourceInput.sourceLines().at(result.error().m_row - 1), result.error());
            m_stack = stackBefore;
            continue;
        }

        if (result.value() != nullptr)
        {
            if (!interpreterVisitor.visit(result.value().get()))
            {
                const Error &error = interpreterVisitor.m_error.value();
                lastError = error;
                printError(m_sourceInput.sourceLines().at(error.m_row - 1), error);
            }
        }
    }

    return 0;
}

Result<std::vector<Token>> Interpreter::fetchTokenLine()
{
    //std::cout << "---> " << __PRETTY_FUNCTION__ << "\n";

    if (!m_rejectedTokens.empty())
    {
        std::cout << "taking one from rejectedTokens. sz: " << m_rejectedTokens.size() << "\n";
        std::vector<Token> tokens = std::move(m_rejectedTokens.back());
        m_rejectedTokens.pop();
        return tokens;
    }

    std::string lineSource = m_sourceInput.fetchLine(m_indentCurrent);

    return m_lexer.fetchLine(m_sourceInput.currentLine(), lineSource);
}

Result<std::vector<Token>::const_iterator> Interpreter::findStatement(TokenIterator itBegin, TokenIterator itEnd) const
{
    auto itStatement = TokenHelper::find(itBegin, itBegin + 2, Token::listStatementTokenTypes);

    if (itStatement == itBegin + 2)
        return itEnd;

    if (itStatement == itBegin)
    {
        //auto itRhs = itBegin + 1;

        switch (itStatement->m_type)
        {
        case TokenType::OpAssignment:
            return Error(itStatement->debugInfo(), "assignment must have an lvalue as left operand");
        case TokenType::If:
        case TokenType::Else:
        case TokenType::While:
        case TokenType::Function:
            return itStatement;
        default:
            break;
        }

    }
    else if (itStatement == itBegin + 1)
    {
        auto itLhs = itBegin;

        switch (itStatement->m_type)
        {
        case TokenType::OpAssignment:
            if (itLhs->m_type == TokenType::Name)
                return itStatement;
            return Error(itStatement->debugInfo(), "assignment must have an lvalue as left operand");
        default:
            break;
        }
    }

    return itEnd;
}


class AutoRec
{
    static int recursionLevel;
public:
    AutoRec() { ++recursionLevel; if (Logging::get().isOn(LogId::Parser)) { std::cout << "RecursionLevel ++ (" << recursionLevel << ")\n"; }; }
    ~AutoRec() { -- recursionLevel; if (Logging::get().isOn(LogId::Parser)) { std::cout << "RecursionLevel -- (" << recursionLevel << ")\n"; }; }
};
int AutoRec::recursionLevel = 0;

Result<StatementBlock::Ptr> Interpreter::fetchStatementBlock()
{
    auto block = std::make_unique<StatementBlock>(DebugInfo(13, 42));

    m_atBlockBegin = true;

    size_t indent = m_indentCurrent;

    forever
    {
        //LOG_ID(LogId::AstCreation, "loop fetchStatementBlock" + std::to_string(indentMin) + " to " + std::to_string(indentMax));
        LOG(LogId::AstCreation, "loop fetchStatementBlock indent: " + std::to_string(m_indentCurrent) + ", atBlockBegin: " + (m_atBlockBegin ? std::string("true") : std::string("false")));

        Result<Statement::Ptr> statement = fetchStatement();
        if (statement.hasError())
            return statement.error();

        if (statement.value() == nullptr)
            LOG(LogId::AstCreation, "got nullllll!!!");
        else
            block->statements.emplace_back(std::move(statement.value()));

        // signal to end block
        if (m_indentCurrent < indent)
        {
            LOG(LogId::AstCreation, "loop fetchStatementBlock indent changed. returning block of " + std::to_string(block->statements.size()));
            return block;
        }

    }

    return block;
}

Result<Statement::Ptr> Interpreter::fetchStatement()
{
    LOG(LogId::AstCreation, "fetchStatement indent: " + std::to_string(m_indentCurrent) + ", atBlockBegin: " + (m_atBlockBegin ? std::string("true") : std::string("false")));

    if (m_sourceInput.isAtEnd() && m_rejectedTokens.empty())
    {
        LOG(LogId::Parser, "reached EOF");
        return Statement::Ptr();
    }

    Result<std::vector<Token>> tokens = fetchTokenLine();
    if (tokens.hasError())
        return tokens.error();

    size_t indent = TokenHelper::getIndentLevel(tokens->cbegin(), tokens->cend());

    // indent error
    if (indent > m_indentCurrent)
        return Error(tokens.value().front().debugInfo(), "indentation error, expected indent below " + std::to_string(m_indentCurrent + 1));

    // indent changed ?
    if (indent != m_indentCurrent)
    {
        // indent error
        if (m_atBlockBegin)
            return Error(tokens.value().front().debugInfo(), "indentation error, expected indent " + std::to_string(m_indentCurrent));

        m_indentCurrent = indent;

        if (indent > 0)
        {
            LOG(LogId::AstCreation, "rejecting statement");
            m_rejectedTokens.emplace(std::move(tokens.value()));
            return Statement::Ptr();
        }
    }

    m_atBlockBegin = false;

    auto itAfterIndent = tokens->cbegin() + indent;

    Result<std::vector<Token>::const_iterator> resultStatement = findStatement(itAfterIndent, tokens->cend());
    if (resultStatement.hasError())
        return resultStatement.error();

    auto itStatement = resultStatement.value();

    if (((tokens->size() == indent + 1) && tokens->back().m_type == TokenType::Empty) || (itAfterIndent == tokens->cend()))
    {
        LOG(LogId::AstCreation, "tokens empty");

        return std::make_unique<StatNoOp>();
    }
    else if (itStatement == tokens->cend())
    {
        LOG(LogId::AstCreation, "no valid statement given");

        // try to parse whole line as expression if no statement given and printing standalone expressions is active so statements are printed in interactive mode
        if (config.m_printStandAloneExpressions)
        {
            Result<Expression::Ptr> resultExpression = parseExpression(itAfterIndent, tokens->cend(), false);

            if (resultExpression.hasValue())
            {
                LOG(LogId::AstCreation, "standalone expression");

                return StatPrint::make(std::move(resultExpression.value()), resultExpression.value()->debugInfo);
            }
        }
        else
        {
            return Error(tokens->front().debugInfo(), "no valid statement given");
        }
    }
    else if (itStatement->m_type == TokenType::OpAssignment)
    {
        LOG(LogId::AstCreation, "found OpAssignment");

        auto itLvalue = (itStatement - 1);

        auto lVal = ExpLValue::make(itLvalue->m_tokenString.str, itLvalue->debugInfo());

        Result<Expression::Ptr> expr = parseExpression((itStatement + 1), tokens->cend(), false);

        if (expr.hasError())
            return expr.error();

        return StatAssignment::make(std::move(lVal), itStatement->m_oAssignment, std::move(expr.value()), itStatement->debugInfo());
    }
    else if (itStatement->m_type == TokenType::While)
    {
        LOG(LogId::AstCreation, "found While");

        Result<Expression::Ptr> condition = parseExpression(itStatement + 1, tokens->cend(), true);
        if (condition.hasError())
            return condition.error();

        ++m_indentCurrent;

        Result<StatementBlock::Ptr> block = fetchStatementBlock();
        if (block.hasError())
            return block.error();

        LOG(LogId::AstCreation, "making While");

        return StatWhile::make(std::move(condition.value()), std::move(block.value()), itStatement->debugInfo());
    }
    else if (itStatement->m_type == TokenType::If)
    {
        LOG(LogId::AstCreation, "found If");

        Result<Expression::Ptr> condition = parseExpression(itStatement + 1, tokens->cend(), true);
        if (condition.hasError())
            return condition.error();

        ++m_indentCurrent;

        Result<StatementBlock::Ptr> block = fetchStatementBlock();
        if (block.hasError())
            return block.error();

        LOG(LogId::AstCreation, "making If");

        return StatIfThenElse::make(std::move(condition.value()), std::move(block.value()), nullptr, itStatement->debugInfo());
    }
    else if (itStatement->m_type == TokenType::Function)
    {
        LOG(LogId::AstCreation, "found Function");

        Result<std::vector<Expression::Ptr>> argList = parseArgList((itStatement + 1), tokens->cend());

        if (argList.hasError())
            return argList.error();

        return StatFunction::make(itStatement->m_tokenString.str, std::move(argList.value()), itStatement->debugInfo());
    }

    LOG(LogId::AstCreation, "returning nullptr");
    return Statement::Ptr();
}

std::optional<Error> Interpreter::errorCheckInput(const std::vector<Token> &tokens)
{
    if (tokens.empty())
        return std::nullopt;

    {   // find some of the possible bad sequences
        auto itLastToken = tokens.begin();

        for (auto it = (tokens.begin() + 1); it != tokens.end(); ++it)
        {
            // empty brackets
            if ((itLastToken->m_type == TokenType::ParenthesesOpen) && (it->m_type == TokenType::ParenthesesClose))
            {
                return Error(itLastToken->debugInfo(), "empty parentheses");
            }

            // name directly following value
            if ((itLastToken->m_type == TokenType::Value) && (it->m_type == TokenType::Name))
            {
                return Error(it->debugInfo(), "expression syntax error");
            }

            // value directly following name
            if ((itLastToken->m_type == TokenType::Name) && (it->m_type == TokenType::Value))
            {
                return Error(it->debugInfo(), "expression syntax error");
            }

            // tokens that cannot follow themselves
            if ((it->m_type == itLastToken->m_type) && !TokenHelper::canFollowItself(it->m_type))
            {
                return Error(it->debugInfo(), "expression syntax error");
            }

            itLastToken = it;
        }
    }

    return std::nullopt;
}


Result<std::vector<Token>> Interpreter::expressionToPostFix(TokenIterator itBegin, TokenIterator itEnd)
{
    std::vector<Token> outputQueue;
    std::stack<Token> operatorStack;

    //std::cout << "\n\n============================\n\n";

    for (TokenIterator it = itBegin; it != itEnd; ++it)
    {
        if ((it->m_type == TokenType::Value) || (it->m_type == TokenType::Name)) // if the token is a number, then
        {
            //std::cout << "it -> out: " << *it << "\n";
            outputQueue.push_back(*it);  // push it to the output queue
        }
        else if (it->m_type == TokenType::Function) // else if the token is a function then
        {
            //std::cout << "it -> stack: " << *it << "\n";
            operatorStack.push(*it);    // push it onto the operator stack
        }
        else if (it->m_type == TokenType::OpBinary) // else if the token is an operator then
        {
            while ((!operatorStack.empty())
                  && ((TokenHelper::getPrecedence(operatorStack.top()) > TokenHelper::getPrecedence(*it))
                   || (TokenHelper::getPrecedence(operatorStack.top()) == TokenHelper::getPrecedence(*it) && TokenHelper::getIsLeftAssociative(*it)))
                  && (operatorStack.top().m_type != TokenType::ParenthesesOpen))
            {
                //std::cout << "stack -> out: " << operatorStack.top() << "\n";
                outputQueue.push_back(operatorStack.top());
                operatorStack.pop();
            }
            //std::cout << "it -> stack: " << *it << "\n";
            operatorStack.push(*it);
        }
        else if (it->m_type == TokenType::ParenthesesOpen)
        {
            //std::cout << "it -> stack: " << *it << "\n";
            operatorStack.push(*it);
        }
        else if (it->m_type == TokenType::ParenthesesClose)
        {
            if (operatorStack.empty())
            {
                // not enough this works:  "x = ( 5 + 6 ("
                return Error(it->debugInfo(), "parentheses mismatch");
            }

            while (operatorStack.top().m_type != TokenType::ParenthesesOpen) // while the operator at the top of the operator stack is not a left parentheses:
            {
                //std::cout << "stack -> out: " << operatorStack.top() << "\n";

                outputQueue.push_back(operatorStack.top());
                operatorStack.pop();    // check if empty parentheses error
            }
            //std::cout << "stack -> discard:  " << operatorStack.top() << "\n";
            operatorStack.pop();    // discard (
        }
        else if (it->m_type == TokenType::Comma)
        {
            // ignore comma
            continue;   // needs some checks or logic 5,5: Assertion `nodes.size() == 1' failed
        }
        else
        {
            return Error(it->m_tokenString.debugInfo.row, it->m_tokenString.debugInfo.col, "unexpected token in expression: " + it->m_tokenString.str);
        }
    }

    while (!operatorStack.empty())
    {
        //std::cout << "stack -> out: " << operatorStack.top() << "\n";
        outputQueue.push_back(operatorStack.top());
        operatorStack.pop();
    }

    //std::cout << "\n\n============================\n\n";

    if (Logging::get().isOn(LogId::Parser))
    {
        std::cout << "\n\n-----------------------\n";

        for (const auto &token : outputQueue)
        {
            std::cout << token.m_tokenString.str << " ";
        }

        std::cout << "\n-----------------------\n\n";
    }

    return outputQueue;
}

Result<Expression::Ptr> Interpreter::parseExpression(TokenIterator itBegin, TokenIterator itEnd, bool parenthesized)
{
    if (parenthesized)
    {
        if (itBegin->m_type != TokenType::ParenthesesOpen)
            return Error(itBegin->debugInfo(), "expected (");

        if ((itEnd - 1)->m_type != TokenType::ParenthesesClose)
            return Error(itBegin->debugInfo(), "expected )");
    }

    // (1 + abs(1) + abs(abs(1 + 1) + 1) + 1)
    // expr_abs(1)

/*
 * // todo: nonvoid functions. how do?
    auto itFn = TokenHelper::find(itBegin, itEnd, TokenType::Function);

    if (itFn != itEnd)
    {
        auto itBeginPar = ++itFn;
        auto itEndPar = TokenHelper::findMatchingEndParenthesis(itBeginPar, itEnd);

//        if (itEndPar == itEnd) // should be covered by source check ?
//            return err;

        Result<Expression::Ptr> exprFn = parseExpression(itBeginPar, itEndPar, true);
    }
*/
    Result<std::vector<Token>> postFix = expressionToPostFix(itBegin, itEnd);

    if (postFix.hasError())
        return postFix.error();

    if (postFix->empty())
        return Expression::Ptr();

    std::stack<Expression::Ptr> nodes;

    for (const Token &token : postFix.value())
    {
        if (token.m_type == TokenType::Value)
        {
            nodes.emplace(ExpValue::make(token.m_value, token.debugInfo()));
        }
        else if (token.m_type == TokenType::Name)
        {
            nodes.emplace(ExpLValue::make(token.m_tokenString.str, token.debugInfo()));
        }
        else if (token.m_type == TokenType::Function)
        {
            auto fn = Function::getFunction(token.m_tokenString.str);

            if (!fn.has_value())
                return Error::genericError();

            std::vector<Expression::Ptr> argList;

            for (size_t i = 0; i < fn.value().argNum; ++i)
            {
                if (nodes.empty())
                    return Error(token.debugInfo(), "function expects " + std::to_string(fn.value().argNum) + " arguments");

                argList.emplace_back(std::move(nodes.top()));
                nodes.pop();
            }

            nodes.emplace(ExpFunction::make(token.m_tokenString.str, std::move(argList), token.debugInfo()));
        }
        else if (token.m_type == TokenType::OpBinary)
        {
            assert(!nodes.empty());

            Expression::Ptr rhs = std::move(nodes.top());
            nodes.pop();

            Expression::Ptr lhs = std::move(nodes.top());
            nodes.pop();

            nodes.emplace(ExpOpBinary::make(std::move(lhs), token.m_opBinary, std::move(rhs), token.debugInfo()));
        }
    }

    // if this fails something is seriously wrong
    assert(nodes.size() == 1);

    return std::move(nodes.top());
}

Result<std::vector<Expression::Ptr>> Interpreter::parseArgList(TokenIterator itBegin, TokenIterator itEnd)
{
    // todo: handle empty ??

    long size = (itEnd - itBegin);

    if (size <= 0)
        return Error(1, 1, "todo: handle empty ??");

    // ckeck begin bracket and min size for also an end bracket
    if ((size < 2) || (itBegin->m_type != TokenType::ParenthesesOpen))
        return Error(itBegin->debugInfo(), "expected argument list");

    // check end bracket
    if (((itEnd - 1)->m_type != TokenType::ParenthesesClose))
        return Error((itEnd - 1)->debugInfo(), "unexpected token at end of argument list");

    // return 0 args
    if (size == 2)
        return std::vector<Expression::Ptr>();

    std::vector<Expression::Ptr> argList;

    ++itBegin; --itEnd;

    while (true)
    {
        auto itComma = TokenHelper::find(itBegin, itEnd, TokenType::Comma);

        Result<Expression::Ptr> expr = parseExpression(itBegin, itComma, false);   // todo, that won't work with 'true' here

        if (expr.hasError())
            return expr.error();

        argList.emplace_back(std::move(expr.value()));

        if (itComma == itEnd)
        {
            return argList;
        }

        itBegin = (itComma + 1);
    }
}

//std::optional<Error> Interpreter::lastError()
//{
//    return interpreterVisitor.m_error;
//}
