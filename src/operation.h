#pragma once

#include <string>
#include <memory>

#include "value.h"
#include "result.h"

namespace Evaluation
{
    extern Result<Value> add(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> substract(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> multiply(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> divide(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);

    extern Result<Value> equal(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> notEqual(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> lessThan(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> greaterThan(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> lessOrEqual(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
    extern Result<Value> greaterOrEqual(const Value &lhs, const Value &rhs, const DebugInfo &debugInfoOp);
}


/*
Value operator+(const Value &lhs, const Value &rhs);
Value operator-(const Value &lhs, const Value &rhs);
Value operator*(const Value &lhs, const Value &rhs);
Value operator/(const Value &lhs, const Value &rhs);

bool operator==(const Value &lhs, const Value &rhs);
bool operator!=(const Value &lhs, const Value &rhs);
bool operator<(const Value &lhs, const Value &rhs);
bool operator>(const Value &lhs, const Value &rhs);
bool operator<=(const Value &lhs, const Value &rhs);
bool operator>=(const Value &lhs, const Value &rhs);
*/
