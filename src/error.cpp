#include "error.h"

#include <sstream>


Error::Error(const DebugInfo &debugInfo, const std::string &msg)
    : m_row(debugInfo.row),
      m_col(debugInfo.col),
      m_msg(msg)
{}

Error::Error(size_t row, size_t col, const std::string &msg)
    : m_row(row),
      m_col(col),
      m_msg(msg)
{}

std::string Error::msg() const
{
    return m_msg;
}
