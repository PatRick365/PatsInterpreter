#include "value.h"

DebugInfo::DebugInfo()
    : row(0),
      col(0)
{}

DebugInfo::DebugInfo(size_t row, size_t col)
    : row(row),
      col(col)
{}

Value Value::null;

Value Value::makeBool(bool b)
{
    return b ? Value(Bool::True) : Value(Bool::False);
}

Value::Type Value::type() const
{
    return std::visit([](auto &&arg) -> Type
    {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, std::monostate>)
            return Type::Null;
        else if constexpr (std::is_same_v<T, Bool>)
            return Type::Bool;
        else if constexpr (std::is_same_v<T, long>)
            return Type::Int;
        else if constexpr (std::is_same_v<T, double>)
            return Type::Float;
        else if constexpr (std::is_same_v<T, std::string>)
            return Type::String;
        else
            static_assert(always_false_v<T>, "non-exhaustive visitor!");
    }, m_value);
}

std::string Value::typeStr() const
{
    return std::visit([](auto &&arg) -> std::string
    {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, std::monostate>)
            return "Null";
        else if constexpr (std::is_same_v<T, Bool>)
            return "Bool";
        else if constexpr (std::is_same_v<T, long>)
            return "Int";
        else if constexpr (std::is_same_v<T, double>)
            return "Float";
        else if constexpr (std::is_same_v<T, std::string>)
            return "String";
        else
            static_assert(always_false_v<T>, "non-exhaustive visitor!");
    }, m_value);
}

std::string Value::toString() const
{
    return std::visit([&](auto &&arg) -> std::string
    {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, std::monostate>)
            return "(null)";
        else if constexpr (std::is_same_v<T, Bool>)
            return (arg == Bool::True) ? "true" : "false";
        else if constexpr (std::is_same_v<T, long>)
            return std::to_string(arg);
        else if constexpr (std::is_same_v<T, double>)
            return std::to_string(arg);
        else if constexpr (std::is_same_v<T, std::string>)
            return std::string("\"") + arg + "\"";
        else
            static_assert(always_false_v<T>, "non-exhaustive visitor!");
    }, m_value);
}

bool Value::isBoolTrue() const
{
    return std::visit([&](auto &&arg) -> bool
    {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, std::monostate>)
            throw std::runtime_error("not bool but: " + toString());
        else if constexpr (std::is_same_v<T, Bool>)
            return (arg == Bool::True);
        else if constexpr (std::is_same_v<T, long>)
            throw std::runtime_error("not bool but: " + toString());
        else if constexpr (std::is_same_v<T, double>)
            throw std::runtime_error("not bool but: " + toString());
        else if constexpr (std::is_same_v<T, std::string>)
            throw std::runtime_error("not bool but: " + toString());
        else
            static_assert(always_false_v<T>, "non-exhaustive visitor!");
    }, m_value);
}

bool Value::isBoolFalse() const
{
    return !isBoolTrue();
}

bool Value::isNumber() const
{
    return std::visit([&](auto &&arg) -> bool
    {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, std::monostate>)
            return false;
        else if constexpr (std::is_same_v<T, Bool>)
            return false;
        else if constexpr (std::is_same_v<T, long>)
            return true;
        else if constexpr (std::is_same_v<T, double>)
            return true;
        else if constexpr (std::is_same_v<T, std::string>)
            return false;
        else
            static_assert(always_false_v<T>, "non-exhaustive visitor!");
    }, m_value);
}

const Value::ValueType &Value::value() const
{
    return m_value;
}

std::ostream& operator<<(std::ostream& os, const Value &value)
{
    os << value.toString();
    return os;
}
