#include "cmdparse.h"

#include <vector>
#include <string>
#include <cassert>
#include <map>

namespace CmdParse
{

static const std::map<char, LogId> maplogCharIds
{
    { 'e', LogId::AstExecution },
    { 'c', LogId::AstCreation },
    { 'p', LogId::Parser },
    { 'l', LogId::Lexer },
    { 'i', LogId::SourceInput },
    { 's', LogId::Stack },
};

std::vector<std::string> getArgList(int argc, char *argv[])
{
    assert(argc > 0);

    std::vector<std::string> args(argc - 1);

    for (int i = 0; i < argc - 1; ++i)
        args[i] = argv[i + 1];

    return args;
}

void printHelp()
{
    std::cout << "usage: Pint\n";

    std::cout << "-l[] logging slots:\n";

    for (const auto &logCharId : maplogCharIds)
    {
        std::cout << "\t" << logCharId.first << ": " << toString(logCharId.second) << "\n";
    }
}

std::vector<std::string>::const_iterator findOption(const std::vector<std::string> &args, const std::string &optionStr)
{
    return std::find_if(args.begin(), args.end(), [&optionStr](const std::string &arg)
    {
        if (arg.find(optionStr) == 0)
            return true;

        return false;
    });
}

std::optional<Options> parsePintConfig(int argc, char *argv[])
{
    std::vector<std::string> args = getArgList(argc, argv);

    std::optional<std::string> filePath;
    std::unordered_set<LogId> logIds;

    if (!args.empty())
    {
        // option: print help
        // if found as any argument, print and exit
        {
            auto it = findOption(args, "-h");
            if (it != args.end())
            {
                printHelp();
                return std::nullopt;
            }
        }

        // option: enable logging
        // set if valid chars otherwise exit
        {
            auto it = findOption(args, "-l");
            if (it != args.end())
            {
                for (const char c : it->substr(2, it->size()))
                {
                    auto itLogIds = maplogCharIds.find(c);

                    if (itLogIds != maplogCharIds.end())
                    {
                        logIds.insert(itLogIds->second);
                    }
                    else
                    {
                        std::cout << "unknown log id \"" << c << "\"\n";
                        printHelp();
                        return std::nullopt;
                    }

                }

                args.erase(it);
            }
        }

        // option: specify file
        // only arg left should be file, try to run it
        {
            if (args.size() == 1)
            {
                filePath = args.front();
            }
            else if (args.size() > 1)
            {
                std::cout << "nope.\n";
                printHelp();
                return std::nullopt;
            }
        }
    }

    return Options { filePath, logIds };
}
}
