#include <iostream>
#include <vector>
#include <string>

#include "config.h"
#include "interpreter.h"
#include "logging.h"
#include "cmdparse.h"


int main(int argc, char *argv[])
{
    if (auto options = CmdParse::parsePintConfig(argc, argv))
    {
        Logging::get().setOn(options->logIds);

//        Logging::get().setOn(LogId::Lexer);
//        Logging::get().setOn(LogId::SourceInput);
//        Logging::get().setOn(LogId::Stack);
//        Logging::get().setOn(LogId::AstExecution);
//        Logging::get().setOn(LogId::AstCreation);
//        Logging::get().setOn(LogId::Parser);

        if (options->file)
        {
            Config config = Config().exitOnError();

            FileSourceCodeInput sourceCodeInput(options->file.value());
            return Interpreter(config, sourceCodeInput).run();
        }
        else
        {
            Config config = Config().printStandAloneExpressions();

            CinSourceCodeInput sourceCodeInput;
            return Interpreter(config, sourceCodeInput).run();
        }
    }
    return 1;
}



// bugs
// if == 0 segfaults
