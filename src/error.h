#pragma once

#include "token.h"


class Error
{
public:
    static Error genericError() { return Error(0, 0, "some error happened..."); }

    Error(const DebugInfo &debugInfo, const std::string &msg);
    Error(size_t row, size_t col, const std::string &msg);

    std::string msg() const;

    size_t m_row;
    size_t m_col;
    std::string m_msg;
};

