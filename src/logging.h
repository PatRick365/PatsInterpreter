#pragma once

#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>

#define LOG(logId, str) do { if (Logging::get().isOn(logId)) { std::cout << toString(logId) << ":" << __FUNCTION__  << " " << str << '\n'; } } while (false)
#define LOG_PRETTY(logId, str) do { if (Logging::get().isOn(logId)) { std::cout << toString(logId) << ":" << __PRETTY_FUNCTION__  << "\n\t" << str << '\n'; } } while (false)
#define LOG_PURE(logId, str) do { if (Logging::get().isOn(logId)) { std::cout << str; } } while (false)

enum class LogId
{
    AstExecution,
    AstCreation,
    Parser,
    Lexer,
    SourceInput,
    Stack
};
std::string toString(LogId logId);

class AutoLogging
{
public:
    AutoLogging(const std::unordered_set<LogId> &logIds);
    ~AutoLogging();

private:
    std::unordered_set<LogId> m_logIdsBefore;
};

class Logging
{
private:
    Logging() = default;

public:
    Logging(const Logging &) = delete;
    Logging &operator=(const Logging &) = delete;
    Logging(Logging &&) = delete;
    Logging &operator=(Logging &&) = delete;

    static Logging &get();

    void setOn(LogId logId, bool on = true);
    void setOn(const std::unordered_set<LogId> &logIds);

    void clear();

    bool isOn(LogId logId) const;
    const std::unordered_set<LogId> &activeLogIds() const;

private:
    std::unordered_set<LogId> m_loggingOn;
};



