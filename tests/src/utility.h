#pragma once

#include "../../src/token.h"

#include <string>
#include <vector>

int randomInt(int from, int to);

std::string randomString(size_t length);

std::vector<Token> randomTokens(size_t length);
