#include "comparators.h"


bool operator==(const Value &lhs, const Value &rhs)
{
    if (auto result = Evaluation::equal(lhs, rhs, DebugInfo()))
    {
        return result->isBoolTrue();
    }
    return false;
}

bool operator!=(const Value &lhs, const Value &rhs)
{
    if (auto result = Evaluation::notEqual(lhs, rhs, DebugInfo()))
    {
        return result->isBoolTrue();
    }
    return false;
}
