﻿#pragma once

#include "utility.h"

#include "../../src/lexer.h"
#include "../../src/logging.h"

#include <gtest/gtest.h>

#ifdef LOG_DURING_TEST
#undef LOG_DURING_TEST
#endif

#define LOG_DURING_TEST AutoLogging log({})
//#define LOG_DURING_TEST AutoLogging log({LogId::Lexer})


TEST(lexer, simple)
{
    LOG_DURING_TEST;

    Lexer lexer;

    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "a = 5");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 3);
        EXPECT_EQ((*tokens)[0].m_tokenString.str, "a");
        EXPECT_EQ((*tokens)[1].m_tokenString.str, "=");
        EXPECT_EQ((*tokens)[2].m_tokenString.str, "5");
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "a=5");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 3);
        EXPECT_EQ((*tokens)[0].m_tokenString.str, "a");
        EXPECT_EQ((*tokens)[1].m_tokenString.str, "=");
        EXPECT_EQ((*tokens)[2].m_tokenString.str, "5");
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, " a=5 ");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 3);
        EXPECT_EQ((*tokens)[0].m_tokenString.str, "a");
        EXPECT_EQ((*tokens)[1].m_tokenString.str, "=");
        EXPECT_EQ((*tokens)[2].m_tokenString.str, "5");
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "  a  =  5  ");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 3);
        EXPECT_EQ((*tokens)[0].m_tokenString.str, "a");
        EXPECT_EQ((*tokens)[1].m_tokenString.str, "=");
        EXPECT_EQ((*tokens)[2].m_tokenString.str, "5");
    }
}

TEST(lexer, string_with_spaces)
{
    ;

    Lexer lexer;

    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, R"(str += "Hello World!")");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 3);
        EXPECT_EQ((*tokens)[0].m_tokenString.str, "str");
        EXPECT_EQ((*tokens)[1].m_tokenString.str, "+=");
        EXPECT_EQ((*tokens)[2].m_tokenString.str, R"("Hello World!")");
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, R"(str += "Hello World!" + " !!! ???")");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 5);
        EXPECT_EQ((*tokens)[0].m_tokenString.str, "str");
        EXPECT_EQ((*tokens)[1].m_tokenString.str, "+=");
        EXPECT_EQ((*tokens)[2].m_tokenString.str, R"("Hello World!")");
        EXPECT_EQ((*tokens)[3].m_tokenString.str, "+");
        EXPECT_EQ((*tokens)[4].m_tokenString.str, R"(" !!! ???")");
    }
}

TEST(lexer, well_formatted)
{
    LOG_DURING_TEST;

    Lexer lexer;

    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, R"(str /= "Hello World!" + 5 - 2 + a * xyz)");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 11);
        int i = 0;
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "str");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "/=");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, R"("Hello World!")");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "+");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "5");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "-");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "2");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "+");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "a");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "*");
        EXPECT_EQ((*tokens)[i++].m_tokenString.str, "xyz");
    }
}


TEST(lexer, bad_formatting)
{
    LOG_DURING_TEST;

    Lexer lexer;

    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, R"(  b /= "Hello World!" - 2334    +  fhj_fesggrE45  )");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 7);
        EXPECT_EQ((*tokens)[0].m_tokenString.str, "b");
        EXPECT_EQ((*tokens)[1].m_tokenString.str, "/=");
        EXPECT_EQ((*tokens)[2].m_tokenString.str, R"("Hello World!")");
        EXPECT_EQ((*tokens)[3].m_tokenString.str, "-");
        EXPECT_EQ((*tokens)[4].m_tokenString.str, "2334");
        EXPECT_EQ((*tokens)[5].m_tokenString.str, "+");
        EXPECT_EQ((*tokens)[6].m_tokenString.str, "fhj_fesggrE45");
    }
}

// should all be moved to parser
TEST(lexer, errors)
{
    LOG_DURING_TEST;

    Lexer lexer;

    {   // unterminated quotes
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, R"(str = "adawdawd)");
        ASSERT_TRUE(tokens.hasError());
        EXPECT_EQ(tokens.error().m_col, 7);
        EXPECT_FALSE(tokens.error().msg().empty());
    }

    // wrong parentheses
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "((5");
        ASSERT_TRUE(tokens.hasError());
        EXPECT_FALSE(tokens.error().msg().empty());
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "5))");
        ASSERT_TRUE(tokens.hasError());
        EXPECT_FALSE(tokens.error().msg().empty());
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "((5)");
        ASSERT_TRUE(tokens.hasError());
        EXPECT_FALSE(tokens.error().msg().empty());
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "((5)))");
        ASSERT_TRUE(tokens.hasError());
        EXPECT_FALSE(tokens.error().msg().empty());
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, ")5(");
        ASSERT_TRUE(tokens.hasError());
        EXPECT_FALSE(tokens.error().msg().empty());
    }
}

TEST(lexer, random_input_no_crash)
{
    //LOG_DURING_TEST;

    Lexer lexer;

    for (size_t i = 1; i <= 1000000; i *= 10)
    {
        auto printableString = randomString(i);
        LOG(LogId::Lexer, printableString);
        auto result = lexer.fetchLine(13, printableString);
        LOG(LogId::Lexer, (result.hasError() ? result.error().msg() : std::string("eyy no error !!")));
    }

    ASSERT_TRUE(true);
}

TEST(lexer, indents)
{
    LOG_DURING_TEST;

    Lexer lexer;

    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "\t");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 1);
        EXPECT_EQ((*tokens).front().m_type, TokenType::Indent);

    }
//    {
//        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "\t");
//        ASSERT_TRUE(tokens.hasError());
//    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "    ");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 1);
        EXPECT_EQ((*tokens).front().m_type, TokenType::Indent);

    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "        ");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 2);
        EXPECT_EQ((*tokens)[0].m_type, TokenType::Indent);
        EXPECT_EQ((*tokens)[1].m_type, TokenType::Indent);
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "            ");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 3);
        EXPECT_EQ((*tokens)[0].m_type, TokenType::Indent);
        EXPECT_EQ((*tokens)[1].m_type, TokenType::Indent);
        EXPECT_EQ((*tokens)[2].m_type, TokenType::Indent);
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "    a = 5");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 4);
        EXPECT_EQ((*tokens)[0].m_type, TokenType::Indent);
        EXPECT_EQ((*tokens)[1].m_type, TokenType::Name);
        EXPECT_EQ((*tokens)[2].m_type, TokenType::OpAssignment);
        EXPECT_EQ((*tokens)[3].m_type, TokenType::Value);

    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "        a = 5");
        ASSERT_FALSE(tokens.hasError());
        ASSERT_EQ(tokens->size(), 5);
        EXPECT_EQ((*tokens)[0].m_type, TokenType::Indent);
        EXPECT_EQ((*tokens)[1].m_type, TokenType::Indent);
        EXPECT_EQ((*tokens)[2].m_type, TokenType::Name);
        EXPECT_EQ((*tokens)[3].m_type, TokenType::OpAssignment);
        EXPECT_EQ((*tokens)[4].m_type, TokenType::Value);
    }
}

