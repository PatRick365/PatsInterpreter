#pragma once

#include "comparators.h"
#include "../../src/ast.h"
#include "../../src/operation.h"
#include "../../src/logging.h"

#include <gtest/gtest.h>

const DebugInfo dbgi(13, 42);

static const Config configAst;

TEST(ast, stack)
{
    //Logging::get().setOn(LogId::Stack);

    Stack stack;
    InterpreterVisitor v(configAst, stack);

    { // x = 5
        auto ass = StatAssignment::make(ExpLValue::make("x", dbgi), OpAssignment::Assign, ExpValue::make(5, dbgi), dbgi);
        ASSERT_TRUE(v.visit(ass.get()));
    }

    { // if x == 5
      //   a = 10
      //   x += a
      // else
      //   a = 20

        //auto ite = std::make_unique<StatIfThenElse>(dbgi);
        //ite->condition = ExpOpBinary::make(ExpLValue::make("x", dbgi), OpBinary::Equal, ExpValue::make(5, dbgi), dbgi);

        auto condition = ExpOpBinary::make(ExpLValue::make("x", dbgi), OpBinary::Equal, ExpValue::make(5, dbgi), dbgi);

        auto ifBlock = std::make_unique<StatementBlock>(dbgi);
        {
            ifBlock->statements.emplace_back(StatAssignment::make(ExpLValue::make("a", dbgi), OpAssignment::Assign, ExpValue::make(10, dbgi), dbgi));
            ifBlock->statements.emplace_back(StatAssignment::make(ExpLValue::make("a2", dbgi), OpAssignment::Assign, ExpValue::make("a2", dbgi), dbgi));
            ifBlock->statements.emplace_back(StatAssignment::make(ExpLValue::make("x", dbgi), OpAssignment::PlusAssign, ExpLValue::make("a", dbgi), dbgi));
        }

        auto elseBranch = StatAssignment::make(ExpLValue::make("a", dbgi), OpAssignment::Assign, ExpValue::make(20, dbgi), dbgi);

        auto ite = StatIfThenElse::make(std::move(condition), std::move(ifBlock), std::move(elseBranch), dbgi);

        ASSERT_TRUE(v.visit(ite.get()));
    }

    ASSERT_EQ(stack.m_scopes.size(), 1);

    ASSERT_TRUE(stack.hasStackValue("x"));
    EXPECT_EQ(stack.findStackValue("x")->value, Value(15));
}

TEST(ast, precedence)
{
    Stack stack;
    InterpreterVisitor v(configAst, stack);

    { // f = (1 + 3) * 4
        // (1 + 3)
        auto bopPlus = ExpOpBinary::make(ExpValue::make(1, dbgi), OpBinary::Plus, ExpValue::make(3, dbgi), dbgi);

        // bopPlus * 4
        auto bopMulti = ExpOpBinary::make(std::move(bopPlus), OpBinary::Multiplication, ExpValue::make(4, dbgi), dbgi);

        // f = bopMulti
        auto ass = StatAssignment::make(ExpLValue::make("f", dbgi), OpAssignment::Assign, std::move(bopMulti), dbgi);

        ASSERT_TRUE(v.visit(ass.get()));
    }

    ASSERT_EQ(v.m_stack.m_scopes.size(), 1);

    ASSERT_TRUE(v.m_stack.hasStackValue("f"));
    EXPECT_EQ(v.m_stack.findStackValue("f")->value, Value(16));
}

TEST(ast, precedence2)
{
    Stack stack;
    InterpreterVisitor v(configAst, stack);

    { // f = ((1 + 3) * 4) * 2
        // (1 + 3)
        auto bopPlus = ExpOpBinary::make(ExpValue::make(1, dbgi), OpBinary::Plus, ExpValue::make(3, dbgi), dbgi);

        // bopPlus * 4
        auto bopMulti1 = ExpOpBinary::make(std::move(bopPlus), OpBinary::Multiplication, ExpValue::make(4, dbgi), dbgi);

        // bopMulti1 * 2
        auto bopMulti2 = ExpOpBinary::make(std::move(bopMulti1), OpBinary::Multiplication, ExpValue::make(2, dbgi), dbgi);

        // f = bopMulti2
        auto ass = StatAssignment::make(ExpLValue::make("f", dbgi), OpAssignment::Assign, std::move(bopMulti2), dbgi);

        EXPECT_TRUE(v.visit(ass.get()));
    }

    ASSERT_EQ(v.m_stack.depth(), 1);

    ASSERT_TRUE(v.m_stack.hasStackValue("f"));
    EXPECT_EQ(v.m_stack.findStackValue("f")->value, Value(32));
}


TEST(ast, assign)
{
//    Logging::get().setOn(LogId::AstExecution);
//    Logging::get().setOn(LogId::Stack);

    Stack stack;
    InterpreterVisitor v(configAst, stack);

    { // x = 1
        auto ass = StatAssignment::make(ExpLValue::make("x", dbgi), OpAssignment::Assign, ExpValue::make(13, dbgi), dbgi);

        EXPECT_TRUE(v.visit(ass.get()));
    }

    ASSERT_EQ(v.m_stack.m_scopes.size(), 1);

    ASSERT_TRUE(v.m_stack.hasStackValue("x"));
    EXPECT_EQ(v.m_stack.findStackValue("x")->value, Value(13));

    { // y = x
        auto ass = StatAssignment::make(ExpLValue::make("y", dbgi), OpAssignment::Assign, ExpLValue::make("x", dbgi), dbgi);
        EXPECT_TRUE(v.visit(ass.get()));
    }

    { // z = x
        auto ass = StatAssignment::make(ExpLValue::make("z", dbgi), OpAssignment::Assign, ExpLValue::make("x", dbgi), dbgi);
        EXPECT_TRUE(v.visit(ass.get()));
    }

    { // z += y
        auto ass = StatAssignment::make(ExpLValue::make("z", dbgi), OpAssignment::PlusAssign, ExpLValue::make("y", dbgi), dbgi);
        EXPECT_TRUE(v.visit(ass.get()));
    }

    ASSERT_TRUE(v.m_stack.hasStackValue("x"));
    EXPECT_EQ(v.m_stack.findStackValue("x")->value, Value(13));

    ASSERT_TRUE(v.m_stack.hasStackValue("y"));
    EXPECT_EQ(v.m_stack.findStackValue("y")->value, Value(13));

    ASSERT_TRUE(v.m_stack.hasStackValue("x"));
    EXPECT_EQ(v.m_stack.findStackValue("x")->value, Value(13));

    ASSERT_TRUE(v.m_stack.hasStackValue("z"));
    EXPECT_EQ(v.m_stack.findStackValue("z")->value, Value(13 + 13));
}

TEST(ast, expr_function_abs)
{
    //AutoLogging log({ LogId::AstExecution });

    Stack stack;
    InterpreterVisitor v(configAst, stack);

    // x = abs(-12)
    std::vector<Expression::Ptr> args;
    args.emplace_back(ExpValue::make(Value(-12), dbgi));

    auto fn = ExpFunction::make("abs", std::move(args), dbgi);
    auto ass = StatAssignment::make(ExpLValue::make("x", dbgi), OpAssignment::Assign, std::move(fn), dbgi);

    EXPECT_TRUE(v.visit(ass.get()));

    ASSERT_EQ(v.m_stack.m_scopes.size(), 1);

    ASSERT_TRUE(v.m_stack.hasStackValue("x"));
    EXPECT_EQ(v.m_stack.findStackValue("x")->value, Value(12));
}

/*
TEST(ast, bop)
{
    Ast a;
    InterpreterVisitor v;

    // x = 3
    // y = 3 + x * 3
    // b = y == 12

    { // x = 3
        auto ass = StatAssignment::make(std::make_unique<LVal>("x"), OpAssignment::Assign, ValueExp::make(3));
        a.root->statements.emplace_back(std::move(ass));
    }

    { // y = 3 + x * 3
        // x * 3
        auto bopMulti = Bop::make(std::make_unique<LVal>("x"), BOperator::Multiplication, ValueExp::make(3));

        // 3 + bopMulti
        auto bopPlus = Bop::make(ValueExp::make(3), BOperator::Plus, std::move(bopMulti));

        // y = bopPlus
        auto ass = StatAssignment::make(std::make_unique<LVal>("y"), OpAssignment::Assign, std::move(bopPlus));
        a.root->statements.emplace_back(std::move(ass));
    }

    { // b1 = y == 12
        auto bopEqual = Bop::make(std::make_unique<LVal>("y"), BOperator::Equal, ValueExp::make(12));

        auto ass = StatAssignment::make(std::make_unique<LVal>("b1"), OpAssignment::Assign, std::move(bopEqual));
        a.root->statements.emplace_back(std::move(ass));
    }

    { // b2 = y == 600
        auto bopEqual = Bop::make(std::make_unique<LVal>("y"), BOperator::Equal, ValueExp::make(600));

        auto ass = StatAssignment::make(std::make_unique<LVal>("b2"), OpAssignment::Assign, std::move(bopEqual));
        a.root->statements.emplace_back(std::move(ass));
    }

    v.visit(a.root.get());

    ASSERT_EQ(v.m_namedValues.size(), 4);
    EXPECT_EQ(v.m_namedValues["x"].value, 3);
    EXPECT_EQ(v.m_namedValues["y"].value, 12);
    EXPECT_EQ(v.m_namedValues["b1"].value, Bool::True);
    EXPECT_EQ(v.m_namedValues["b2"].value, Bool::False);
}


TEST(ast, if_else)
{
    Ast a;
    InterpreterVisitor v;

    { // x = 5
        auto ass = StatAssignment::make(std::make_unique<LVal>("x"), OpAssignment::Assign, ValueExp::make(5));
        a.root->statements.emplace_back(std::move(ass));
    }

    { // if x == 5
      //   a = 10
      //   a2 = "a2"
      // else
      //   a = 20
        auto ite = std::make_unique<IfThenElse>();
        ite->condition = Bop::make(std::make_unique<LVal>("x"), BOperator::Equal, ValueExp::make(5));

        auto ifBlock = std::make_unique<StatementBlock>();
        {
            ifBlock->statements.emplace_back(StatAssignment::make(std::make_unique<LVal>("a"), OpAssignment::Assign, ValueExp::make(10)));
            ifBlock->statements.emplace_back(StatAssignment::make(std::make_unique<LVal>("a2"), OpAssignment::Assign, ValueExp::make("a2")));
            ite->ifBranch = std::move(ifBlock);
        }

        ite->elseBranch = StatAssignment::make(std::make_unique<LVal>("a"), OpAssignment::Assign, ValueExp::make(20));
        a.root->statements.emplace_back(std::move(ite));
    }

    { // if x == 5
      //   b = 10
      // else
      //   b = 20
        auto ite = std::make_unique<IfThenElse>();
        ite->condition = Bop::make(std::make_unique<LVal>("x"), BOperator::Equal, ValueExp::make(666));
        ite->ifBranch = StatAssignment::make(std::make_unique<LVal>("b"), OpAssignment::Assign, ValueExp::make(10));
        ite->elseBranch = StatAssignment::make(std::make_unique<LVal>("b"), OpAssignment::Assign, ValueExp::make(20));
        a.root->statements.emplace_back(std::move(ite));
    }

    v.visit(a.root.get());

    ASSERT_EQ(v.m_namedValues.size(), 4);
    EXPECT_EQ(v.m_namedValues["x"].value, 5);
    EXPECT_EQ(v.m_namedValues["a"].value, 10);
    EXPECT_EQ(v.m_namedValues["a2"].value, Value("a2"));
    EXPECT_EQ(v.m_namedValues["b"].value, 20);
}

TEST(ast, statement_while)
{
    Ast a;
    InterpreterVisitor v;

    { // x = 0
        auto ass = StatAssignment::make(std::make_unique<LVal>("x"), OpAssignment::Assign, ValueExp::make(0));
        a.root->statements.emplace_back(std::move(ass));
    }

    {
        auto statWhile = std::make_unique<StatWhile>();
        statWhile->condition = Bop::make(std::make_unique<LVal>("x"), BOperator::LessThan, ValueExp::make(10));
        statWhile->body = StatAssignment::make(std::make_unique<LVal>("x"), OpAssignment::PlusAssign, ValueExp::make(1));
        a.root->statements.emplace_back(std::move(statWhile));
    }

    v.visit(a.root.get());

    ASSERT_EQ(v.m_namedValues.size(), 1);
    EXPECT_EQ(v.m_namedValues["x"].value, 10);
}

//    StatAssignment::Ptr initStatement;
//    Expression::Ptr condition;
//    Statement::Ptr iterationExpression; // stat or exp ???
//    Statement::Ptr body;

TEST(ast, statement_for)
{
    Ast a;
    InterpreterVisitor v;

    { // x = 0
        auto ass = StatAssignment::make(std::make_unique<LVal>("x"), OpAssignment::Assign, ValueExp::make(0));
        a.root->statements.emplace_back(std::move(ass));
    }


    // for i = 1 i <= 10 i +=1
    //   x -= 1

    auto statFor = std::make_unique<StatFor>();

    // i = 1
    statFor->initStatement = StatAssignment::make(std::make_unique<LVal>("i"), OpAssignment::Assign, ValueExp::make(0));

    // i < 10
    statFor->condition = Bop::make(std::make_unique<LVal>("i"), BOperator::LessThanOrEqual, ValueExp::make(10));

    // i +=1
    statFor->iterationExpression = StatAssignment::make(std::make_unique<LVal>("i"), OpAssignment::PlusAssign, ValueExp::make(1));

    // x -= 1
    statFor->body = StatAssignment::make(std::make_unique<LVal>("x"), OpAssignment::MinusAssign, ValueExp::make(1));
    a.root->statements.emplace_back(std::move(statFor));

    v.visit(a.root.get());

    ASSERT_EQ(v.m_namedValues.size(), 2);
    EXPECT_EQ(v.m_namedValues["x"].value, -10);
    EXPECT_EQ(v.m_namedValues["i"].value, 10);

}
*/
// for i = x i < y i +=1
