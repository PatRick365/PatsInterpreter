#pragma once

#include "utility.h"

#include "../../src/lexer.h"
#include "../../src/logging.h"

#include <gtest/gtest.h>

#ifdef LOG_DURING_TEST
#undef LOG_DURING_TEST
#endif

#define LOG_DURING_TEST AutoReleaseLogging log({})
//#define LOG_DURING_TEST AutoReleaseLogging log({LogId::Parser})


TEST(parser, simple)
{

}

TEST(parser, checkInput)
{

    Lexer lexer;
    Parser parser;

    {
        // empty parentheses
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "()");
        ASSERT_TRUE(tokens.hasValue());

        Result<Statement::Ptr> result = parser.parseStatement(tokens.value());
        ASSERT_TRUE(result.hasError());
        EXPECT_EQ(result.error().m_col, 1);
        EXPECT_FALSE(result.error().msg().empty());
    }

    // stuff that makes no sense to follow each other
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "5 ++ 5");
        ASSERT_TRUE(tokens.hasValue());

        Result<Statement::Ptr> result = parser.parseStatement(tokens.value());
        ASSERT_TRUE(result.hasError());
        EXPECT_FALSE(result.error().msg().empty());
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "5 5");
        ASSERT_TRUE(tokens.hasValue());

        Result<Statement::Ptr> result = parser.parseStatement(tokens.value());
        ASSERT_TRUE(result.hasError());
        EXPECT_FALSE(result.error().msg().empty());
    }
    {
        Result<std::vector<Token>> tokens = lexer.fetchLine(13, "5 +=+=");
        ASSERT_TRUE(tokens.hasValue());

        Result<Statement::Ptr> result = parser.parseStatement(tokens.value());
        ASSERT_TRUE(result.hasError());
        EXPECT_FALSE(result.error().msg().empty());
    }



}




TEST(parser, random_input_no_crash)
{
    LOG_DURING_TEST;

    Parser parser;

    for (size_t i = 0; i < 1000; ++i)
    {
        for (size_t j = 1; j <= 10; ++j)
        {
            auto tokens = randomTokens(j);
            //LOG_ID(LogId::Lexer, printableString);
            auto result = parser.parseStatement(tokens);
            //LOG_ID(LogId::Lexer, (result.hasError() ? result.error().msg() : std::string("eyy no error !!")));

            //LOG_ID(LogId::Lexer, toString(tokens));

            //std::cout << toString(tokens) << "\n";

            //LOG_ID(LogId::Lexer, toString(tokens));

            if (result.hasValue())
                LOG_ID(LogId::Parser, "eyy no error !!");
            else
                LOG_ID(LogId::Parser, result.error().msg());
        }
    }

    ASSERT_TRUE(true);
}

