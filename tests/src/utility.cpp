#include "utility.h"

#include <random>

int randomInt(int from, int to)
{
    thread_local static std::random_device rd; // obtain a random number from hardware
    thread_local static std::mt19937 gen(rd()); // seed the generator

    std::uniform_int_distribution<> distr(from, to); // define the range

    return distr(gen);
}

std::string randomString(size_t length)
{
    static auto& chrs = R"(" !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜø£Ø×ƒáíóúñÑªº¿®¬½¼¡«»░▒▓│┤ÁÂÀ©╣║╗╝¢¥┐└┴┬├─┼ãÃ╚╔╩╦╠═╬¤ðÐÊËÈıÍÎÏ┘┌█▄¦Ì▀ÓßÔÒõÕµþÞÚÛÙýÝ¯´≡±‗¾¶§÷¸°¨·¹³²■)";

    thread_local static std::mt19937 rg{std::random_device{}()};
    thread_local static std::uniform_int_distribution<size_t> pick(0, sizeof(chrs) - 2);

    std::string s;

    s.reserve(length);

    while(length--)
        s += chrs[pick(rg)];

    return s;
}

std::string randomAlphaString(size_t length)
{
    static auto& chrs = R"("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ)";

    thread_local static std::mt19937 rg{std::random_device{}()};
    thread_local static std::uniform_int_distribution<size_t> pick(0, sizeof(chrs) - 2);

    std::string s;

    s.reserve(length);

    while(length--)
        s += chrs[pick(rg)];

    return s;
}

Value randomValue()
{
    Value::Type type = Value::listTypes[randomInt(0, Value::listTypes.size())];

    switch (type)
    {
    case Value::Type::Null:
        return Value();
    case Value::Type::Bool:
        return randomInt(0, 1) ? Value(Bool::False) : Value(Bool::True);
    case Value::Type::Int:
        return Value(int(randomInt(0, 9999999)));
    case Value::Type::Float:
        return Value(float((randomInt(0, 9999999) / 3.14)));
    case Value::Type::String:
        return Value(randomAlphaString(randomInt(0, 30)));
    }

    throw "Press F";
}

std::vector<Token> randomTokens(size_t length)
{
//    Token(const TokenString &tokenString, TokenType type);
//    Token(const TokenString &tokenString, const Value &value);
//    Token(const TokenString &tokenString, OpBinary op);
//    Token(const TokenString &tokenString, OpAssignment op);

    std::vector<Token> vec;

    for (size_t i = 0; i < length; ++i)
    {
        //TokenType tokenType = Token::listStatementTokenTypes[randomInt(0, Token::listStatementTokenTypes.size())];
        OpBinary opBinary = listOpBinary[randomInt(0, listOpBinary.size())];
        OpAssignment opAssignment = listOpAssignment[randomInt(0, listOpAssignment.size())];

        switch (randomInt(1, 3))
        {
//        case 0:
//        {
//            vec.push_back(Token(TokenString("abc", DebugInfo()), tokenType));
//            break;
//        }
        case 1:
        {
            vec.push_back(Token(TokenString("13XXX13", DebugInfo()), randomValue()));
            break;
        }
        case 2:
        {
            vec.push_back(Token(TokenString("/", DebugInfo()), opBinary));
            break;
        }
        case 3:
        {
            vec.push_back(Token(TokenString("*=", DebugInfo()), opAssignment));
            break;
        }
        }
    }

    return vec;

}
