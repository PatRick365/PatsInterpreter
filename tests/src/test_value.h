#pragma once

#include "comparators.h"
#include "../../src/value.h"
#include "../../src/operation.h"

#include <gtest/gtest.h>


TEST(value, construct)
{
    Value vLong1(5);
    Value vDouble1(3.14);
    Value vString1("blahh");

    Value vLong2(vLong1);

    EXPECT_EQ(vLong1, vLong2);

    Value vLong3;
    vLong3 = vLong1;
    EXPECT_EQ(vLong1, vLong3);

    Value val(5);
    val = "5";


    EXPECT_NE(val, Value(5));
    EXPECT_EQ(val, Value("5"));
}

TEST(value, toString)
{
    EXPECT_STREQ(Value().toString().data(), "(null)");
    EXPECT_STREQ(Value(5).toString().data(), "5");
    EXPECT_STREQ(Value(3.14).toString().data(), "3.140000");
    EXPECT_STREQ(Value("blahh").toString().data(), R"("blahh")");
}

TEST(value, operatorEqual)
{
    Value vNull;
    Value vLong5(5);
    Value vLong50(50);
    Value vDouble(3.14);
    Value vString("blahh");

    EXPECT_EQ(vNull, Value());
    EXPECT_EQ(vLong5, Value(5));
    EXPECT_EQ(vDouble, Value(3.14));
    EXPECT_EQ(vString, Value("blahh"));

    EXPECT_EQ(vLong5, 5);
    EXPECT_EQ(vDouble, 3.14);
    EXPECT_EQ(vString, "blahh");

    EXPECT_NE(vLong5, Value(3));
    EXPECT_NE(vDouble, Value(3.14 * 2));
    EXPECT_NE(vString, Value("uffff"));
    EXPECT_NE(vNull, vLong5);
    EXPECT_NE(vNull, vDouble);
    EXPECT_NE(vNull, vString);
    EXPECT_NE(vLong5, vLong50);
    EXPECT_NE(vLong5, vDouble);
    EXPECT_NE(vLong5, vString);
}
