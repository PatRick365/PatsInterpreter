#pragma once

#include "../../src/interpreter.h"
#include "../../src/operation.h"
#include "../../src/logging.h"

#include <gtest/gtest.h>

#include <fstream>
#include <filesystem>

// todo move these tests to test_interpreter

TEST(test_complete, simple)
{
    //Logging::get().setOn(LogId::Stack);
    Logging::get().setOn(LogId::Lexer);
    //Logging::get().setOn(LogId::AstExecution);

    Interpreter pint;

    std::string path("simple.pint");
    std::ofstream file(path);

    ASSERT_TRUE(file.is_open());

    file <<
R"(
a = 5
b = a * 3
{
    c = 42
    a /= 2
}

str1 = \"hello_world\""
str2 = \"hello world\""
bool = 12 > 10
)";

    file.close();

    pint.runFile(path);

    ASSERT_EQ(pint.stack.m_scopes.size(), 1);

    // todo replace nullptr comparison with hasValue

    ASSERT_TRUE(pint.stack.findStackValue("a") != nullptr);
    EXPECT_TRUE(Evaluation::equal(pint.stack.findStackValue("a")->value, Value(2.5)));

    ASSERT_TRUE(pint.stack.findStackValue("b") != nullptr);
    EXPECT_TRUE(Evaluation::equal(pint.stack.findStackValue("b")->value, Value(15)));

    ASSERT_TRUE(pint.stack.findStackValue("c") == nullptr);

    ASSERT_TRUE(pint.stack.findStackValue("str1") != nullptr);
    EXPECT_TRUE(Evaluation::equal(pint.stack.findStackValue("str1")->value, Value("hello_world")));

    ASSERT_TRUE(pint.stack.findStackValue("str2") != nullptr);
    EXPECT_TRUE(Evaluation::equal(pint.stack.findStackValue("str2")->value, Value("hello world")));

    auto v = pint.stack.findStackValue("str")->value;
    std::cout << "v->" << v << "\n";

    ASSERT_TRUE(pint.stack.findStackValue("bool") != nullptr);
    EXPECT_TRUE(Evaluation::equal(pint.stack.findStackValue("bool")->value, Bool::True));
    EXPECT_TRUE(Evaluation::notEqual(pint.stack.findStackValue("bool")->value, Bool::False));

    ASSERT_TRUE(std::filesystem::remove(path));
}
