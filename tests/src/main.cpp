#include "test_value.h"
#include "test_lexer.h"
#include "test_ast.h"
//#include "test_parser.h"
#include "test_utility.h"
#include "test_interpreter.h"

//#include "test_complete.h"

#include <gtest/gtest.h>

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
