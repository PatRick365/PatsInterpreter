#pragma once

#include "../../src/interpreter.h"
#include "../../src/operation.h"
#include "../../src/logging.h"
#include "comparators.h"

#include <gtest/gtest.h>

#include <fstream>
#include <filesystem>

static const Config configInterpreter;

TEST(test_interpreter, variable_declaration)
{
    std::vector<std::string> code
    {
        "i = 5",
        "b=true",
        "str = \"abc\"",
        "f   =   0.12"
    };


    StringSourceCodeInput sourceCodeInput(configInterpreter, code);
    Interpreter pint(configInterpreter, sourceCodeInput);

    ASSERT_EQ(pint.run(), 0);

    ASSERT_EQ(pint.m_stack.depth(), 1);
    ASSERT_EQ(pint.m_stack.valueCount(), 4);

    ASSERT_TRUE(pint.m_stack.hasStackValue("i"));
    EXPECT_EQ(pint.m_stack.findStackValue("i")->value, Value(5));

    ASSERT_TRUE(pint.m_stack.hasStackValue("b"));
    EXPECT_EQ(pint.m_stack.findStackValue("b")->value, Value(Bool::True));

    ASSERT_TRUE(pint.m_stack.hasStackValue("f"));
    EXPECT_EQ(pint.m_stack.findStackValue("f")->value, Value(0.12));
}

TEST(test_interpreter, if_1)
{

    std::vector<std::string> code
    {
        "a = 1",
        "if (a > 0)",
        "    a = 3"
    };


    StringSourceCodeInput sourceCodeInput(configInterpreter, code);
    Interpreter pint(configInterpreter, sourceCodeInput);

    ASSERT_EQ(pint.run(), 0);

    ASSERT_EQ(pint.m_stack.depth(), 1);
    ASSERT_EQ(pint.m_stack.valueCount(), 1);

    ASSERT_TRUE(pint.m_stack.hasStackValue("a"));
    EXPECT_EQ(pint.m_stack.findStackValue("a")->value, Value(3));
}

TEST(test_interpreter, if_2)
{
    //Logging::get().setOn(LogId::Parser);

    std::vector<std::string> code
    {
        "a = 1",
        "b = 0",
        "",
        "if (a > 0)",
        "    ",
        "    a = 3",
        //"",  // no good
        "    ",
        "    b = 5",
        "    "
    };


    StringSourceCodeInput sourceCodeInput(configInterpreter, code);
    Interpreter pint(configInterpreter, sourceCodeInput);

    ASSERT_EQ(pint.run(), 0);

    ASSERT_EQ(pint.m_stack.depth(), 1);
    EXPECT_EQ(pint.m_stack.valueCount(), 2);

    ASSERT_TRUE(pint.m_stack.hasStackValue("a"));
    EXPECT_EQ(pint.m_stack.findStackValue("a")->value, Value(3));

    ASSERT_TRUE(pint.m_stack.hasStackValue("b"));
    EXPECT_EQ(pint.m_stack.findStackValue("b")->value, Value(5));

}


TEST(test_interpreter, non_void_function)
{
    //Logging::get().setOn(LogId::Parser);

    std::vector<std::string> code
    {
        "i = 5 - 10",
        "print(\"println i = \")",
        "println(i)",
        "r = abs(i)",
        "m = min(10, 999)",
    };

    StringSourceCodeInput sourceCodeInput(configInterpreter, code);
    Interpreter pint(configInterpreter, sourceCodeInput);

    ASSERT_EQ(pint.run(), 0);

    ASSERT_EQ(pint.m_stack.depth(), 1);
    ASSERT_EQ(pint.m_stack.valueCount(), 3);

    ASSERT_TRUE(pint.m_stack.hasStackValue("i"));
    EXPECT_EQ(pint.m_stack.findStackValue("i")->value, Value(-5));

    ASSERT_TRUE(pint.m_stack.hasStackValue("r"));
    EXPECT_EQ(pint.m_stack.findStackValue("r")->value, Value(5));

    ASSERT_TRUE(pint.m_stack.hasStackValue("m"));
    EXPECT_EQ(pint.m_stack.findStackValue("m")->value, Value(10));
}

TEST(test_interpreter, function_not_enough_args)
{
    //Logging::get().setOn(LogId::Parser);

    std::vector<std::string> code
    {
        "m = min(10)",
    };

    StringSourceCodeInput sourceCodeInput(configInterpreter, code);
    Interpreter pint(configInterpreter, sourceCodeInput);

    ASSERT_EQ(pint.run(), 0);

    ASSERT_TRUE(pint.lastError.has_value());
}

TEST(test_interpreter, function_wrong_type_arg)
{
    //Logging::get().setOn(LogId::Parser);

    std::vector<std::string> code
    {
        "m = min(\"eyy\", 3)",
    };

    StringSourceCodeInput sourceCodeInput(configInterpreter, code);
    Interpreter pint(configInterpreter, sourceCodeInput);

    ASSERT_EQ(pint.run(), 0);

    ASSERT_TRUE(pint.lastError.has_value());
}

TEST(test_interpreter, semicolon_crash)
{
    Logging::get().setOn(LogId::Parser);
    Logging::get().setOn(LogId::Lexer);
    Logging::get().setOn(LogId::AstCreation);

    std::vector<std::string> code
    {
        //"println(\"hello! whats your name ?\");"
        "i = abs(3);"
    };

    StringSourceCodeInput sourceCodeInput(configInterpreter, code);
    Interpreter pint(configInterpreter, sourceCodeInput);

    ASSERT_EQ(pint.run(), 0);

    ASSERT_EQ(pint.m_stack.depth(), 1);
    ASSERT_EQ(pint.m_stack.valueCount(), 1);

}

